/**
 * @brief Program that runs a GUI to THDM_complex.
 * 
 * To compile one must have gtkmm-3.0  installed.
 * Compile with flag `pkg-config gtkmm-3.0 --cflags --libs`
 */

#include "GUI.h"

#include <gtkmm/application.h>
#include <iostream>

int main(int argc, char *argv[])
{
    
    auto app = Gtk::Application::create(argc,argv,"GUI.beta");

    GUI gui;
  
    return app->run(gui);
}  