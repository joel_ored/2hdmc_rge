/**=============================================================================
 * @brief: ParameterScan class
 * @author: Joel Oredsson
 *
 * Parameter scan class that can be used to scan the parameter space of a 2HDM
 *
 *============================================================================*/

#include "ParameterScan.h"
#include "HelpFunctions.h"
#include "SPheno.h"
#include "Structures.h"

#ifdef MINUIT
#include "THDM_fitter.h"
#endif

#include <algorithm>
#include <atomic>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using std::cout;
using std::vector;
using std::string;
using std::complex;

void evolve_thdm_vector(vector<THDM_complex> &thdmVec)
{
  for (auto &thdm : thdmVec)
  {
    thdm.evolve();
  }
  // Timer timer("evolve_thdmVec:");

  // static const unsigned int numThreads = std::thread::hardware_concurrency();
  // cout << numThreads << " concurrent threads are supported.\n";

  // cout << "\nStarting to process models on " << numThreads
  //           << " threads (each evolving " << thdmVec.size() / numThreads
  //           << " parameter points)...\n";

  // // std::atomic<unsigned int> count = 0;

  // vector<std::thread> threads;
  // for (unsigned int i = 1; i <= numThreads; ++i)
  // {
  //   threads.push_back(
  //       std::thread( thread_work_evolve, thdmVec) );//count,
  //                   // vector<THDM_complex>(
  //                   //     thdmVec.begin() + (i - 1) * thdmVec.size() /
  //                   numThreads,
  //                   //     thdmVec.begin() + i * thdmVec.size() /
  //                   numThreads)));

  //   // To prevent cout from overlapping.
  //   std::this_thread::sleep_for(std::chrono::milliseconds(10));
  // }

  // // Wait for all threads to evolve their thdm models.
  // for (;;)
  // {
  //   // cout.width(20);
  //   // cout << std::flush;

  //   // cout << "\r"
  //   //           << " Count: ";
  //   // cout.width(12);
  //   // cout << count << std::flush;

  //   std::this_thread::sleep_for(std::chrono::milliseconds(10));

  //   // if (count >= thdmVec.size())
  //     break;
  // }

  // for (unsigned int i = 0; i < threads.size(); ++i)
  // {
  //   threads[i].join();
  // }

  // cout <<
  // "============================================================\n"; cout
  // << "\nAll " << numThreads
  //           << " threads have joined! Process of thdm is complete!\n";
}

void thread_work_evolve(
    vector<THDM_complex> &thdmVec) // std::atomic<int> &count,
{
  for (auto &thdm : thdmVec)
  {
    thdm.set_logLevel(LOG_ERRORS);
    thdm.evolve();
    // ++count;
  }
}

//----ScanSystem-class----------------------------------------------------------

ScanSystem::ScanSystem(const int totalThreads)
    : _scanType(ScanType::EXACT_Z2), _fermionSector(UP), _z2Symmetry(TYPE_I),
      _minEnergy(2e2), _runSpheno(true), _useMinuit(false), _massLoopLvl(1),
      _scanDataFile("parameterPoints.dat"),
      _scanDataFile_1loop("parameterPoints_1loop.dat"),
      _scanDataFile_generic("parameterPoints_generic.dat"),
      _scanDataFile_generic_1loop_evolved("parameterPoints_generic_1loop_evolved.dat"),
      _scanDataFile_generic_evolved("parameterPoints_generic_evolved.dat"),
      _scanDataFile_higgs("parameterPoints_higgs.dat"),
      _scanDataFile_invariant("parameterPoints_invariant.dat"),
      _scanDataFile_physical("parameterPoints_physical.dat"),
      _scanDataFile_spheno("parameterPoints_spheno.dat"),
      _scanDataFile_yukawa("parameterPoints_yukawa.dat"),
      _scanDataFile_evolved("parameterPoints_evolved.dat"),
      _scanDataFile_1loop_evolved("parameterPoints_1loop_evolved.dat"),
      _description(""), _directory("unknownScan"), _threads(totalThreads),
      _maxPoints(10), _pointsFound(0), _quitScan(false),
      _startTime(std::chrono::high_resolution_clock::now()), _v2(60624.5686)
{
  if (totalThreads > 8)
  {
    printf("[ERROR]: The maximum number of threads for a scan is 8.");
    _threads = 8;
  }
}

ScanSystem::~ScanSystem()
{
  // cout << "Destroying ScanSystem!\n";
}

void ScanSystem::set_rgeConfig(const RgeConfig &rgeConfig)
{
  _rgeConfig = rgeConfig;
}

void ScanSystem::set_max_points(const int maxPoints) { _maxPoints = maxPoints; }

void ScanSystem::set_directory(const string &directory)
{
  _directory = directory;
}

void ScanSystem::set_yukawa_type(Z2symmetry type)
{
  _hybRange.yukawaType = type;
}

void ScanSystem::create_data_files()
{
  cout << "Results stored in "
            << "output/" + _directory << std::endl;
  _files.set_directory("output/" + _directory);

  // Creating folder for SLHA files
  if (_createModelfiles)
    create_folder_path("output/" + _directory + "/modelFiles");

  /* Creates and opens the data  files. This will overwrite
      any file with the same name in the directory. To change that
      add the option "true". */
  _files.create_file(_scanDataFile);
  _files.create_file(_scanDataFile_1loop);
  _files.create_file(_scanDataFile_generic);
  _files.create_file(_scanDataFile_higgs);
  _files.create_file(_scanDataFile_invariant);
  _files.create_file(_scanDataFile_physical);
  _files.create_file(_scanDataFile_evolved);
  _files.create_file(_scanDataFile_1loop_evolved);
  _files.create_file(_scanDataFile_generic_1loop_evolved);
  _files.create_file(_scanDataFile_generic_evolved);

  if (_scanType == ALIGNED_YUKAWA || _scanType == SOFTLY_BROKEN_Z2_ALIGNED)
  {
    _files.create_file(_scanDataFile_yukawa);
    _files.add_line(_scanDataFile_yukawa,
                    vector<string>{"#real(aU)", "imag(aU)",
                                             "real(aD)", "imag(aD)", "real(aL)",
                                             "imag(aL)"});
  }
  if (_runSpheno)
  {
    _files.create_file(_scanDataFile_spheno);
    _files.add_line(_scanDataFile_spheno,
                    vector<string>{"#SPheno:mh", "mH", "mA", "mHc",
                                             "S", "T", "U"});
  }

  /* Adds comment line to describe the columns in the data files. */
  _files.add_line(_scanDataFile,
                  vector<string>{"#ef", "ef_pert", "ef_unit",
                                           "ef_stab", "efViolation"});
  _files.add_line(_scanDataFile_1loop,
                  vector<string>{"#(1-loop)ef", "ef_pert", "ef_unit",
                                           "ef_stab", "efViolation"});

  _files.add_line(_scanDataFile_higgs,
                  vector<string>{"#xi", "beta", "tan(beta)", "Y1",
                                           "Y2", "real(Y3)", "imag(Y3)", "Z1",
                                           "Z2", "Z3", "Z4", "real(Z5)",
                                           "imag(Z5)", "real(Z6)", "imag(Z6)",
                                           "real(Z7)", "imag(Z7)"});

  vector<string> genFileComment = {"#xi", "beta", "tan(beta)", "M112", "M222", "real(M12)",
                      "imag(M12)", "Lambda1", "Lambda2", "Lambda3", "Lambda4",
                      "real(Lambda5)", "imag(Lambda5)", "real(Lambda6)",
                      "imag(Lambda6)", "real(Lambda7)", "imag(Lambda7)"};

  _files.add_line(_scanDataFile_generic, genFileComment);
  _files.add_line(_scanDataFile_generic_evolved, genFileComment);
  _files.add_line(_scanDataFile_generic_1loop_evolved, genFileComment);

  _files.add_line(_scanDataFile_invariant,
                  vector<string>{"#xi", "beta", "tan(beta)", "cPhi",
                                           "mHc", "mh[0]", "mh[1]", "mh[2]",
                                           "s12", "c13", "Z2", "Z3", "theta23",
                                           "real(Z7inv)", "imag(Z7inv)"});

  _files.add_line(
      _scanDataFile_physical,
      vector<string>{"#Tree-lvl:mh", "mH", "mA", "cba", "sba", "v"});

  _files.add_line(_scanDataFile_evolved,
                  vector<string>{
                      "#StopScaleValues:mu",
                      "abs(z2Quantity)", "real(z2Quantity)", "imag(z2Quantity)",
                      "max(rF(i,j))", "->|value|", "max(lamF(i,j))",
                      "->|value|", "maxLambda", "->|value|", "lamD23"});

  _files.add_line(_scanDataFile_1loop_evolved,
                  vector<string>{
                      "#StopScaleValues:mu", "reM12", "reLambda6", "reLambda7",
                      "abs(z2Quantity)", "real(z2Quantity)", "imag(z2Quantity)",
                      "max(rF(i,j))", "->|value|", "max(lamF(i,j))",
                      "->|value|", "maxLambda", "->|value|", "lamD23"});

  create_parameter_info_file();

  _files.close_streams();
}

void ScanSystem::set_scan_description(const string &description)
{
  _description = description;
}

void ScanSystem::process_model(const THDM_complex &model)
{
  /**
   * Algorithm:
   *
   * 1.) Set up range for parameter scan.
   * 2.) Divide up scan. Multiple threads.
   * 3.) Print data and plots.
   */

  _scanType = MODEL_PROCESS;

  /* Sets directory of _files and creates data files. */
  // create_data_files();

  Base_invariant inv = model.get_param_invariant();

  // static double delta_s12 = 0.1;
  static double delta_Z = 0.4;
  static int step = 4;
  static double stepSizeZ = 2. * delta_Z / step;

  // Total number of points to evolve
  _maxPoints = (step + 1) * (step + 1) * (step + 1);

  complex<double> Z7inv = inv.Z7inv;
  double Z2 = inv.Z2;
  double Z3 = inv.Z3;

  vector<THDM_complex> thdmVec;

  thdmVec.resize((step + 1) * (step + 1) * (step + 1));

  int num = 0;
  for (int i = 0; i <= step; ++i)
  {
    inv.Z2 = Z2 - delta_Z + i * stepSizeZ;
    for (int j = 0; j <= step; ++j)
    {
      inv.Z3 = Z3 - delta_Z + j * stepSizeZ;
      for (int k = 0; k <= step; ++k)
      {
        inv.Z7inv = complex<double>(real(Z7inv) - delta_Z + k * stepSizeZ,
                                         imag(Z7inv));
        thdmVec[num].set_param_invariant(inv);
        thdmVec[num].set_logLevel(LOG_ERRORS);
        thdmVec[num].set_rgeConfig(_rgeConfig);
        thdmVec[num++].set_yukawa_type(model.get_yukawa_type());
      }
    }
  }

  evolve_thdm_vector(thdmVec);
}

vector<vector<double>>
ScanSystem::load_folders_base_invariant(const string &dir) const
{
  // Loading model from invariant basis data file.
  // Columns: #mh1 mh2	mh3	mHc	tan(beta)	s12	c13	cPhi
  // Z2 Z3 real(Z7exp(-itheta23))	imag(Z7exp(-itheta23)) theta23
  string file = "output/" + dir + "/parameterPoints_invariant.dat";
  cout << "Loading " << file << std::endl;

  // Loads the file into ifstream
  std::ifstream modelFileStream(file, std::ios_base::in);

  /* Saving the file as a vector with all the rows. */
  vector<vector<double>> fileVec;
  vector<double> rowVec;

  if (!modelFileStream)
  {
    cout << "[ERROR]: Couldn't open " << file << std::endl;
    return fileVec;
  }

  // Converting to sStream
  std::stringstream sStream;
  sStream << modelFileStream.rdbuf();

  int columns = 15; // THIS MUST MATCH NUMBER OF COLUMNS IN FILE!
  rowVec.resize(columns);

  /* Throwing away first row that is just comments. */
  string temp;
  for (int i = 0; i < columns; ++i)
    sStream >> temp;

  for (;;)
  {
    // Reading each row
    for (int i = 0; i < columns; ++i)
      sStream >> rowVec[i];

    // Breaks when sStream is empty
    if (!sStream)
      break;

    // Saving the row
    fileVec.push_back(rowVec);
  }

  cout << "File contains " << fileVec.size() << " parameter points.\n";
  return fileVec;
}

void ScanSystem::process_folder(const string &dir)
{
  /**
   * Algorithm:
   *
   * 1.) Load file containing the parameter points.
   * 2.) Save all points as a Base_invariant object.
   * 3.) Divide up and distribute among multiple threads.
   * 4.) Each thread evolves and stores result of the
   *   parameter points given.
   */

  cout << "Processing collection of THDM parameter points!\n";

  _directory = dir + "/" + _rgeConfig.evolutionName;
  create_data_files();

  SM_RGE sm;
  const char *smFile =
      realpath(("output/" + dir + "/sm_modelFile.txt").c_str(), NULL);
  if (!_files.load_model_file(sm, smFile))
    return;
  create_sm_modelFile(sm);

  cout << "Loading THDM_complex objects!\n";

  // Loading the parameter points into a vector
  vector<vector<double>> fileVec = load_folders_base_invariant(dir);

  vector<THDM_complex> thdmVec;
  thdmVec.resize(fileVec.size());

  Base_invariant inv;
  for (unsigned int i = 0; i < fileVec.size(); ++i)
  {
    thdmVec[i].set_sm(sm);
    thdmVec[i].set_rgeConfig(_rgeConfig);
    thdmVec[i].set_logLevel(LOG_ERRORS);

    inv.mh[0] = fileVec[i][0];
    inv.mh[1] = fileVec[i][1];
    inv.mh[2] = fileVec[i][2];
    inv.mHc = fileVec[i][3];
    inv.beta = atan(fileVec[i][4]);
    inv.s12 = fileVec[i][5];
    inv.c13 = fileVec[i][6];
    inv.Z2 = fileVec[i][8];
    inv.Z3 = fileVec[i][9];
    inv.Z7inv = complex<double>(fileVec[i][10], fileVec[i][11]);
    inv.theta23 = fileVec[i][12];

    thdmVec[i].set_param_invariant(inv);
    thdmVec[i].set_yukawa_type(_hybRange.yukawaType);
  }

  evolve_thdmVec(thdmVec);
}

void ScanSystem::process_folder_1loop(const string &dir)
{
  /**
   * Algorithm:
   *
   * 1.) Load parameterPoints_inv.dat in "dir" containing the parameter points.
   * 2.) Save all points in a THDM_complex vector.
   * 3.) Divide up and distribute among multiple threads.
   * 4.) Each thread evolves and stores result of the
   *   parameter points given.
   */

  cout << "1-loop evolution of THDM parameter points!\n";

  RgeConfig rgeConfig = _rgeConfig;
  rgeConfig.evolutionName = "1loop";
  rgeConfig.twoloop = false;

  _directory = dir + "/" + rgeConfig.evolutionName;
  create_data_files();

  SM_RGE sm;
  const char *smFile =
      realpath(("output/" + dir + "/sm_modelFile.txt").c_str(), NULL);
  if (!_files.load_model_file(sm, smFile))
    return;
  create_sm_modelFile(sm);

  // Loading the parameter points into a vector
  vector<vector<double>> fileVec = load_folders_base_invariant(dir);

  vector<THDM_complex> thdmVec;
  thdmVec.resize(fileVec.size());

  Base_invariant inv;
  for (unsigned int i = 0; i < fileVec.size(); ++i)
  {
    thdmVec[i].set_sm(sm);
    thdmVec[i].set_rgeConfig(rgeConfig);
    thdmVec[i].set_logLevel(LOG_ERRORS);

    inv.beta = atan(fileVec[i][1]);
    inv.mHc = fileVec[i][4];
    inv.mh[0] = fileVec[i][5];
    inv.mh[1] = fileVec[i][6];
    inv.mh[2] = fileVec[i][7];
    inv.s12 = fileVec[i][8];
    inv.c13 = fileVec[i][9];
    inv.Z2 = fileVec[i][10];
    inv.Z3 = fileVec[i][11];
    inv.theta23 = fileVec[i][12];
    inv.Z7inv = complex<double>(fileVec[i][13], fileVec[i][14]);

    if (!thdmVec[i].set_param_invariant(inv))
      cout << "[ERROR]: Couldn't set invariant basis!\n";
    thdmVec[i].set_yukawa_type(_hybRange.yukawaType);
  }

  evolve_thdmVec(thdmVec);
}

void ScanSystem::evolve_thdmVec(vector<THDM_complex> &thdmVec)
{
  vector<std::thread> threads;
  _maxPoints = thdmVec.size();
  cout << "\nStarting to evolve thdmVec on " << _threads
            << " threads (each evolving " << thdmVec.size() / _threads
            << " parameter points)...\n";

  for (int i = 1; i <= _threads; ++i)
  {
    threads.push_back(
        std::thread(&ScanSystem::thread_work_thdmVec, this, i - 1,
                    vector<THDM_complex>(
                        thdmVec.begin() + (i - 1) * thdmVec.size() / _threads,
                        thdmVec.begin() + i * thdmVec.size() / _threads)));

    // To prevent cout from overlapping.
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  // Wait for all threads to evolve their thdm models.
  for (;;)
  {

    if (_pointsFound >= _maxPoints)
      break;

    cout << "\nEvolving models...\n";
    cout << "Progress: " << (int)(100 * (float)_pointsFound / _maxPoints)
              << " %\n";
    print_time();
    cout << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(10));
  }

  for (unsigned int i = 0; i < threads.size(); i++)
  {
    threads[i].join();
  }

  cout << "============================================================\n";
  cout << "\nAll " << _threads
            << " threads have joined! Process of thdm is complete!\n";
  print_time();
}

void ScanSystem::hard_z2_breaking(const string &dir)
{
  _description = "/**\n"
                 "*Hard Z2 breaking of softly broken Z2 scan\n"
                 "*/\n";
  // Algorithm:
  //   1.) Load file containing the parameter points.
  //   2.) Save all points as a Base_invariant object.
  //   3.) Divide up and distribute among multiple threads.
  //   4.) Each thread evolves and stores result of the
  //     parameter points given.

  cout
      << "\n------------------------------------------------------------\n"
      << "Hard Z2 breaking of softly broken Z2 scan\n"
      << "------------------------------------------------------------\n\n";

  _directory = dir + "/HARD_Z2_BREAKING";
  create_data_files();

  SM_RGE sm;
  const char *smFile =
      realpath(("output/" + dir + "/sm_modelFile.txt").c_str(), NULL);

  if (!_files.load_model_file(sm, smFile))
    return;

  create_sm_modelFile(sm);
  _v2 = sm.get_v2();

  gsl_rng *rng = create_gsl_rng();

  // Loading the parameter points into a vector
  vector<vector<double>> fileVec = load_folders_base_invariant(dir);

  vector<THDM_complex> thdmVec;
  thdmVec.resize(fileVec.size());
  Base_invariant inv;
  double Z2, Z3;
  double_range ZRange(-M_PI, M_PI);

  // Creating THDM models in
  for (unsigned int i = 0; i < fileVec.size(); ++i)
  {
    inv.mh[0] = fileVec[i][0];
    inv.mh[1] = fileVec[i][1];
    inv.mh[2] = fileVec[i][2];
    inv.mHc = fileVec[i][3];
    inv.beta = atan(fileVec[i][4]);
    inv.s12 = fileVec[i][5];
    inv.c13 = fileVec[i][6];
    inv.Z7inv = complex<double>(fileVec[i][10], fileVec[i][11]);
    inv.theta23 = fileVec[i][12];

    thdmVec[i].set_sm(sm);
    thdmVec[i].set_rgeConfig(_rgeConfig);
    thdmVec[i].set_logLevel(LOG_ERRORS);

    int numTries = 0;
    static const int maxTries = 10000;
    for (;;)
    {
      // Breaking the Z2 hard, by shifting Z2 and Z3..
      Z2 = ZRange.min + (ZRange.max - ZRange.min) * gsl_rng_uniform(rng);
      Z3 = ZRange.min + (ZRange.max - ZRange.min) * gsl_rng_uniform(rng);
      inv.Z2 = fileVec[i][8] + Z2;
      inv.Z3 = fileVec[i][9] + Z3;

      if (!thdmVec[i].set_param_invariant(inv))
        continue;

      if (!thdmVec[i].delete_imag_parts())
        continue;

      thdmVec[i].set_yukawa_type(_hybRange.yukawaType);

      if (thdmVec[i].is_pert_unit_stab() && thdmVec[i].is_cp_conserved())
      {
        // cout << "Broken hard, i = " << i << std::endl;
        break;
      }
      else if (numTries++ > maxTries)
      {
        cout << "Couldn't break the symmetry hard in good way. maxTries "
                     "reached\n";
        break;
      }
    }
  }

  // Free memory allocation
  gsl_rng_free(rng);

  evolve_thdmVec(thdmVec);
}

void ScanSystem::hard_scalar_breaking(const string &dir)
{

  cout << "\n------------------------------------------------------------";
  cout << "\n Hard scalar breaking of Z_2\n";
  cout << " using SLHA files in " << dir << std::endl;
  cout << "------------------------------------------------------------\n";

  vector<THDM_complex> thdmVec = load_model_files("output/" + dir);
  cout << "\nLoaded " << thdmVec.size() << " modelfiles!\n";

  // Generating small hard scalar breaking
  gsl_rng *rng = create_gsl_rng();

  static const double_range lambdaRange(-1., 1.);

  for (auto &thdm : thdmVec)
  {
    Base_generic gen = thdm.get_param_gen();
    gen.Lambda6 = complex<double>(lambdaRange.min +
                                           (lambdaRange.max - lambdaRange.min) *
                                               gsl_rng_uniform(rng),
                                       0.);
    gen.Lambda7 = complex<double>(lambdaRange.min +
                                           (lambdaRange.max - lambdaRange.min) *
                                               gsl_rng_uniform(rng),
                                       0.);
    thdm.set_param_gen(gen, false);
    thdm.set_rgeConfig(_rgeConfig);
#ifdef MINUIT
    Base_generic genFitted = fit_to_125mh(thdm, 0);
    thdm.set_param_gen(genFitted, false);
#endif
  }
  // Free memory allocation
  gsl_rng_free(rng);

  // Evolve the new THDM_complex objects
  evolve_thdm_vector(thdmVec);

  // Save the evolved THDM_complex objects
  create_folder_path("output/" + _directory + "/modelFiles");
  int numModels = 0;
  for (auto &thdm : thdmVec)
  {
    thdm.write_slha_file(-1, "output/" + _directory + "/modelFiles/thdm_" +
                                 std::to_string(++numModels) + ".txt");
  }
}

void ScanSystem::soft_z2_breaking(const string &dir)
{
  _description = "/**\n"
                 "* Soft Z2 breaking of exact Z2 scan\n"
                 "*/\n";
  // Algorithm:
  //   1.) Load file containing the the exact Z2 symmetric parameter points.
  //   2.) Save all points as a Base_invariant object.
  //       Break Z2 symmetry by generating random Z7 coupling
  //   3.) Divide up and distribute among multiple threads.
  //   4.) Each thread evolves and stores result of the
  //     parameter points given.

  cout
      << "\n------------------------------------------------------------\n"
      << "Soft Z2 breaking of exact Z2 scan\n"
      << "------------------------------------------------------------\n\n";

  // Loading the parameter points into a vector
  vector<vector<double>> fileVec = load_folders_base_invariant(dir);

  _directory = dir + "/SOFT_Z2_BREAKING";

  create_data_files();

  SM_RGE sm;
  const char *smFile =
      realpath(("output/" + dir + "/sm_modelFile.txt").c_str(), NULL);
  if (!_files.load_model_file(sm, smFile))
    return;
  create_sm_modelFile(sm);
  _v2 = sm.get_v2();

  gsl_rng *rng = create_gsl_rng();

  vector<THDM_complex> thdmVec;
  thdmVec.resize(fileVec.size());
  Base_invariant inv;
  double_range ZRange(-M_PI, M_PI);

  for (unsigned int i = 0; i < fileVec.size(); ++i)
  {
    thdmVec[i].set_sm(sm);
    thdmVec[i].set_rgeConfig(_rgeConfig);
    thdmVec[i].set_logLevel(LOG_ERRORS);

    // Parameters that are the same in softly broken case
    inv.mh[0] = fileVec[i][0];
    inv.mh[1] = fileVec[i][1];
    inv.mh[2] = fileVec[i][2];
    inv.mHc = fileVec[i][3];
    inv.beta = atan(fileVec[i][4]);
    inv.s12 = fileVec[i][5];
    inv.c13 = fileVec[i][6];
    inv.theta23 = fileVec[i][12];

    // Breaking the Z2 symmetry softly by making Z7 a free parameter.

    for (;;)
    {
      // First retrieve symmetric Z7, then add deviation.
      inv.Z7inv = complex<double>(fileVec[i][10], fileVec[i][11]);

      complex<double> deltaZ7 = complex<double>(
          ZRange.min + (ZRange.max - ZRange.min) * gsl_rng_uniform(rng), 0.);
      deltaZ7 *= std::polar(1., -inv.theta23);

      inv.Z7inv += deltaZ7;

      // After shifting Z7, we must recalculate Z2 and Z3 to still be in a hard
      // Z2 symmetric 2HDM. Must determine Z1, Z4, Z5, Z6, Z7 in the real Higgs
      // basis first.
      double tan2b = tan(2. * inv.beta);
      double cot2b = 1. / tan2b;

      // The square of the sin of angles. (sign is to be determined below)
      double s122 = inv.s12 * inv.s12;
      double s132 = 1. - inv.c13 * inv.c13;

      double c12 = sqrt(1. - s122);

      double Z1 = (inv.mh[0] * inv.mh[0] * c12 * c12 * inv.c13 * inv.c13 +
                   inv.mh[1] * inv.mh[1] * s122 * inv.c13 * inv.c13 +
                   inv.mh[2] * inv.mh[2] * s132) /
                  _v2;

      // Fixing the signs
      double s13 = sign(Z1 * _v2 - inv.mh[2] * inv.mh[2]) * sqrt(s132);

      /* Setting up diagonal mass matrix and rotation matrix, eq.44, in
    Phys.Rev.D 74, 015018. */
      Eigen::Matrix3d rot, mass, massDiag;
      rot.setZero();
      massDiag.setZero();

      rot(0, 0) = c12 * inv.c13;
      rot(0, 1) = -inv.s12;
      rot(0, 2) = -c12 * s13;
      rot(1, 0) = inv.c13 * inv.s12;
      rot(1, 1) = c12;
      rot(1, 2) = -inv.s12 * s13;
      rot(2, 0) = s13;
      rot(2, 1) = 0.;
      rot(2, 2) = inv.c13;

      for (int i = 0; i < 3; ++i)
        massDiag(i, i) = inv.mh[i] * inv.mh[i];

      mass = rot.transpose() * massDiag * rot;
      double A2 = mass(2, 2);
      complex<double> Z5inv = complex<double>(
          (mass(1, 1) - A2) / _v2, (-2. * mass(1, 2)) / _v2);
      complex<double> Z6inv =
          complex<double>(mass(0, 1) / _v2, -mass(0, 2) / _v2);
      double Z4 = real(Z5inv) + (2. * (A2 - inv.mHc * inv.mHc)) / _v2;

      // Going to Higgs basis specified by theta23
      complex<double> detU = std::polar(1., inv.theta23);
      double Z5 = real(detU * detU * Z5inv);
      double Z6 = real(detU * Z6inv);
      double Z7 = real(detU * inv.Z7inv);

      // Z2 and Z3 are fixed by Z_2 symmetry.
      inv.Z2 = Z1 + 2. * (Z6 + Z7) * cot2b;
      inv.Z3 = Z1 - Z4 - Z5 + 2. * cot2b * Z6 - (Z6 - Z7) * tan2b;

      if (!thdmVec[i].set_param_invariant(inv))
        continue;

      if (!thdmVec[i].delete_imag_parts())
        continue;

      thdmVec[i].set_yukawa_type(_hybRange.yukawaType);

      if (thdmVec[i].is_pert_unit_stab() && thdmVec[i].is_cp_conserved())
        break;
    }
  }

  // Free memory allocation
  gsl_rng_free(rng);

  evolve_thdmVec(thdmVec);
}

void ScanSystem::set_minimum_energy(const double energy)
{
  _minEnergy = energy;
}

void ScanSystem::run_rge_scan()
{
  cout << "\nStarting new scan!\n";
  print_scanType();

  create_data_files();

  // Creates _threads number of threads which runs thread_work until
  // _quitScan = true.
  vector<std::thread> threads;
  for (int i = 0; i < _threads; i++)
  {
    cout << "Starting new thread!\n";
    threads.push_back(std::thread(&ScanSystem::thread_work_scan, this, i));

    // To prevent cout from overlapping.
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  cout << "============================================================\n";

  // Scan loop
  for (;;)
  {

    if (_pointsFound >= _maxPoints)
    {
      _quitScan = true;
      break;
    }

    print_status();
    std::this_thread::sleep_for(std::chrono::seconds(30));
  }

  // _quitScan is now true and all the threads should join.
  for (unsigned int i = 0; i < threads.size(); i++)
    threads[i].join();

  cout << "============================================================\n";
  cout << "\nAll " << _threads
            << " threads have joined! Process of thdm is complete!\n";
  print_time();
  _quitScan = false;
}

void ScanSystem::thread_work_scan(const int threadID)
{
  gsl_rng *rng = create_gsl_rng(threadID);

  cout << "Starting thread " << threadID + 1 << " !\n";

  SM_RGE sm;
  // sm.evolve_to(500.);
  create_sm_modelFile(sm);

  THDM_complex thdm(sm);
  thdm.set_logLevel(LOG_ERRORS);
  // RgeConfig rgeConfigTemp = _rgeConfig;

  thdm.set_rgeConfig(_rgeConfig);
  thdm.save_current_state();

  // Scan loop
  while (!_quitScan)
  {
    generate_point(thdm, rng);
    THDM_complex thdmUnevolved(thdm);

#ifdef MINUIT
    if (_useMinuit)
      improve_parameters(thdm); // Uses Minuit to reach higher energies
#endif

    if (!thdm.evolve())
      continue; // evolve() returns false if encountering NaN value.

    if (satisfies_requirements(thdm))
    {
      print_to_files(thdmUnevolved, thdm, threadID);

      if (_createModelfiles)
        create_model_file(thdm);
    }
  }

  // Free memory allocation
  gsl_rng_free(rng);
}

void ScanSystem::thread_work_thdmVec(const int threadID,
                                     vector<THDM_complex> thdmVec)
{
  for (auto &thdm : thdmVec)
  {
    thdm.set_logLevel(LOG_ERRORS);
 
    if (_runSpheno)
      thdm.run_spheno(1);

    THDM_complex thdmUnevolved(thdm);

    if (thdm.evolve())
    {
      print_to_files(thdmUnevolved, thdm, threadID);
    }
    else
    {
      cout << "[ERROR]: Couldn't evolve THDM in thdmVec.\n";
      ++_pointsFound;
    }
  }
}

void ScanSystem::print_time() const
{
  std::chrono::time_point<std::chrono::high_resolution_clock> nowTime =
      std::chrono::high_resolution_clock::now();
  std::chrono::duration<float> duration = nowTime - _startTime;

  float sec = duration.count();
  int min = (int)floor((int)sec / 60);
  int hours = (int)floor(min / 60);
  min %= 60;
  sec = sec - 60 * 60 * hours - 60 * min;

  printf("Time: %i h %i m %.f s.\n", hours, min, sec);
}

void ScanSystem::print_average_point_time() const
{
  std::chrono::time_point<std::chrono::high_resolution_clock> nowTime =
      std::chrono::high_resolution_clock::now();
  std::chrono::duration<float> duration = nowTime - _startTime;

  float sec = duration.count();
  float secPerPoint = sec / (float)_pointsFound;
  int minPerPoint = (int)floor((int)secPerPoint / 60);
  int hoursPerPoint = (int)floor(minPerPoint / 60);
  minPerPoint %= 60;
  secPerPoint = secPerPoint - 60 * 60 * hoursPerPoint - 60 * minPerPoint;

  printf("1 point per %i h %i m %.3f s.\n", hoursPerPoint, minPerPoint,
         secPerPoint);

  float secLeft = _maxPoints * secPerPoint - sec;
  int minLeft = (int)floor((int)secLeft / 60);
  int hoursLeft = (int)floor(minLeft / 60);
  minLeft %= 60;
  secLeft = secLeft - 60 * 60 * hoursLeft - 60 * minLeft;

  printf("Estimated time left: %i h %i m %.f s.\n", hoursLeft, minLeft,
         secLeft);
}

void ScanSystem::set_scanType(const ScanType &scanType)
{
  _scanType = scanType;
}

void ScanSystem::set_aF_scan(const FermionSector &fermionSector,
                             const Z2symmetry &z2Symmetry)
{
  _fermionSector = fermionSector;
  _z2Symmetry = z2Symmetry;
}

void ScanSystem::set_hybrid_basis_range(const Range_hybrid &hybRange)
{
  _hybRange = hybRange;
}

void ScanSystem::set_base_generic(const Base_generic &gen) { _gen = gen; }

void ScanSystem::set_spheno_config(const bool runSpheno,
                                   const int massLoopLvl)
{
  _runSpheno = runSpheno;
  _massLoopLvl = massLoopLvl;
}

void ScanSystem::use_minuit(const bool useMinuit) { _useMinuit = useMinuit; }

void ScanSystem::create_model_files(const bool createModelFiles)
{
  _createModelfiles = createModelFiles;
};

void ScanSystem::generate_point(THDM_complex &thdm, const gsl_rng *rng) const
{
  switch (_scanType)
  {
  case EXACT_Z2:
  {
    /**
     * @brief EXACT_Z2_ALIGNED
     *
     * 6 free parameters: mh, mH, cba, tanb, Z4, Z5.
     *
     * CP is conserved.
     *
     * Scanning flat angle distribution for tanb.
     *
     * We use the Z2 symmetric hybrid basis.
     * Z7 is fixed by eq.51, 52 and 58 in arXiv:1507.04281 respectively.
     */

    thdm.reset_to_saved_state();

    // Ranges for the parameters:
    static const double_range betaRange = {atan(_hybRange.tanb.min),
                                           atan(_hybRange.tanb.max)};

    Base_hybrid hyb;

    // Generates a random parameter point that is perturbative, unitary and
    // stable.
    for (;;)
    {
      // Random free parameters
      hyb.mh = _hybRange.mh.min +
               (_hybRange.mh.max - _hybRange.mh.min) * gsl_rng_uniform(rng);
      hyb.mH = _hybRange.mH.min +
               (_hybRange.mH.max - _hybRange.mH.min) * gsl_rng_uniform(rng);
      hyb.cba = _hybRange.cba.min +
                (_hybRange.cba.max - _hybRange.cba.min) * gsl_rng_uniform(rng);
      double beta = betaRange.min +
                    (betaRange.max - betaRange.min) * gsl_rng_uniform(rng);
      hyb.Z4 = _hybRange.Z4.min +
               (_hybRange.Z4.max - _hybRange.Z4.min) * gsl_rng_uniform(rng);
      hyb.Z5 = _hybRange.Z5.min +
               (_hybRange.Z5.max - _hybRange.Z5.min) * gsl_rng_uniform(rng);

      double sba = sqrt(1. - hyb.cba * hyb.cba);
      hyb.tanb = tan(beta);
      double tan2b = tan(2. * beta);
      double cot2b = 1. / tan2b;

      // Z7 is fixed by the exact Z2 symmetry (free for softly broken).
      hyb.Z7 = (hyb.mh * hyb.mh - hyb.mH * hyb.mH) * sba * hyb.cba +
               2. * cot2b *
                   (hyb.mH * hyb.mH * sba * sba +
                    hyb.mh * hyb.mh * hyb.cba * hyb.cba);
      hyb.Z7 /= thdm.get_v2();

      Base_generic gen = hyb.convert_to_generic(thdm.get_v2());

      if (!thdm.set_param_gen(gen))
        continue;

      thdm.set_yukawa_type(_hybRange.yukawaType);

      // thdm.set_logLevel(LOG_WARNINGS); // DEBUG

      if (parameter_point_is_okay(thdm))
      {
        break;
      }
    }
    break;
  }
  case SOFTLY_BROKEN_Z2:
  {
    /**
     * @brief SOFTLY_BROKEN_Z2
     *
     * 7 free parameters: mh, mH, cba, tanb, Z4, Z5, Z7
     *
     * CP is conserved.
     *
     * Scanning flat angle distribution for tanb.
     *
     * We use the Z2 symmetric hybrid basis.
     */

    thdm.reset_to_saved_state();

    Base_hybrid hyb;

    // Generates a random parameter point that is perturbative, unitary and
    // stable.
    for (;;)
    {
      hyb = _hybRange.get_random_point( rng);

      if (!thdm.set_param_hybrid(hyb))
        continue;

      thdm.set_yukawa_type(_hybRange.yukawaType);

      // thdm.set_logLevel(LOG_WARNINGS); //DEBUG

      if (parameter_point_is_okay(thdm))
      {
        break;
      }
    }
    break;
  }
  case HARD_BROKEN_Z2:
  {
    /**
     * @brief HARD_BROKEN_Z2
     *
     * 9 free parameters: mh, mH, cba, tanb, Z2, Z3, Z4, Z5, Z7
     *
     * CP is conserved.
     *
     * Scanning flat angle distribution for tanb.
     *
     * We use the Z2 symmetric hybrid basis as a first step. Then adding
     * deviations of Lambda6 and Lambda7 in generic basis.
     */

    thdm.reset_to_saved_state();

    Base_hybrid hyb;

    // static const double_range lambdaRange(-0.5 * M_PI, 0.5 * M_PI);
    static const double_range lambdaRange_log(log(1e-5), log(2.));
    double lambda6_log, lambda7_log;

    // Generates a random parameter point that is perturbative, unitary and
    // stable.
    for (;;)
    {
      // Random free softly broken Z2 parameters
      hyb = _hybRange.get_random_point(rng);

      Base_generic gen = hyb.convert_to_generic(thdm.get_v2());

      lambda6_log =
          lambdaRange_log.min +
          (lambdaRange_log.max - lambdaRange_log.min) * gsl_rng_uniform(rng);

      gen.Lambda6 = complex<double>(exp(lambda6_log), 0.);
      if (gsl_rng_uniform(rng) < 0.5)
        gen.Lambda6 *= -1.;

      lambda7_log =
          lambdaRange_log.min +
          (lambdaRange_log.max - lambdaRange_log.min) * gsl_rng_uniform(rng);

      gen.Lambda7 = complex<double>(exp(lambda7_log), 0.);
      if (gsl_rng_uniform(rng) < 0.5)
        gen.Lambda7 *= -1.;

      // gen.Lambda6 = complex<double>(
      //     lambdaRange.min +
      //         (lambdaRange.max - lambdaRange.min) * gsl_rng_uniform(rng),
      //     0.);
      // gen.Lambda7 = complex<double>(
      //     lambdaRange.min +
      //         (lambdaRange.max - lambdaRange.min) * gsl_rng_uniform(rng),
      //     0.);

      if (!thdm.set_param_gen(gen))
        continue;

      thdm.set_yukawa_type(_hybRange.yukawaType);

      // thdm.set_logLevel(LOG_WARNINGS); //DEBUG

      if (parameter_point_is_okay(thdm))
      {
        break;
      }
    }
    break;
  }
  case ALIGNED_YUKAWA:
  {
    /**
     * @brief ALIGNED_YUKAWA
     *
     * Free parameters: aF = rF/kF. Which one is set by _fermionSector.
     * The other ones are set by _z2Symmetry.
     */

    thdm.reset_to_saved_state();

    // Ranges for the parameters:
    static const double_range aURange_log = {std::log(0.001), std::log(10.)};
    static const double_range aDLRange_log = {std::log(0.001), std::log(100.)};

    double aF;
    double aF_log;
    double tanb = tan(_gen.beta);
    double cotb = 1. / tanb;

    // Generates a random parameter point that is perturbative, unitary and
    // stable.
    for (;;)
    {
      if (!thdm.set_param_gen(_gen))
      {
        cout
            << "[ERROR]: Can't set the generic base in ALIGNED_YUKAWA scan.\n";
        continue;
      }

      // Flat distribution in log(aF)
      if (_fermionSector == UP)
      {
        aF_log = aURange_log.min +
                 (aURange_log.max - aURange_log.min) * gsl_rng_uniform(rng);
      }
      else
      {
        aF_log = aDLRange_log.min +
                 (aDLRange_log.max - aDLRange_log.min) * gsl_rng_uniform(rng);
      }

      if (_fermionSector != UP && gsl_rng_uniform(rng) < 0.5)
      {
        aF = -std::exp(aF_log); // Include negative values for aD and aL
      }
      else
      {
        aF = std::exp(aF_log);
      }
      vector<double> aFVec; // Fills up with (aU, aD, aL) below.

      switch (_z2Symmetry)
      {
      case TYPE_I:
      {
        aFVec.push_back(cotb); // aU
        aFVec.push_back(cotb); // aD
        aFVec.push_back(cotb); // aL
        break;
      }
      case TYPE_II:
      {
        aFVec.push_back(cotb);  // aU
        aFVec.push_back(-tanb); // aD
        aFVec.push_back(-tanb); // aL
        break;
      }
      case TYPE_III:
      {
        aFVec.push_back(cotb);  // aU
        aFVec.push_back(-tanb); // aD
        aFVec.push_back(cotb);  // aL
        break;
      }
      case TYPE_IV:
      {
        aFVec.push_back(cotb);  // aU
        aFVec.push_back(cotb);  // aD
        aFVec.push_back(-tanb); // aL
        break;
      }
      
      default:
      {
        cout << "[ERROR]: Invalid _z2Symmetry set for ScanSystem.\n";
        break;
      }
      }

      // Exchange symmetrical value for free parameter
      aFVec[(int)_fermionSector] = aF;

      thdm.set_yukawa_aligned(aFVec[0], aFVec[1], aFVec[2]);

      // thdm.set_logLevel(LOG_WARNINGS); //DEBUG

      if (parameter_point_is_okay(thdm))
      {
        break;
      }
    }
    break;
  }
  case SOFTLY_BROKEN_Z2_ALIGNED:
  {
    /**
     * @brief SOFTLY_BROKEN_Z2_ALIGNED
     *
     * 8 free parameters: mh, mH, cba, Z4, Z5, Z7, aU, aD
     *
     * CP is conserved. 
     * Aligned Yukawa sector with random aU, aD (tanb is fixed).
     *
     * Scanning flat angle distribution for tanb.
     *
     * We use the Z2 symmetric hybrid basis.
     */

    thdm.reset_to_saved_state();

    // Ranges for the parameters:
    static const double_range aURange = {-1., 1.};
    static const double_range aDRange = {-50., 50.};
    

    Base_hybrid hyb;
    double aU, aD, aL;

    hyb.tanb = tan(_gen.beta);
    
    // Generates a random parameter point that is perturbative, unitary and
    // stable.
    for (;;)
    {
      // Random free parameters
      hyb.mh = _hybRange.mh.min +
               (_hybRange.mh.max - _hybRange.mh.min) * gsl_rng_uniform(rng);
      hyb.mH = _hybRange.mH.min +
               (_hybRange.mH.max - _hybRange.mH.min) * gsl_rng_uniform(rng);
      hyb.cba = _hybRange.cba.min +
                (_hybRange.cba.max - _hybRange.cba.min) * gsl_rng_uniform(rng);
      hyb.Z4 = _hybRange.Z4.min +
               (_hybRange.Z4.max - _hybRange.Z4.min) * gsl_rng_uniform(rng);
      hyb.Z5 = _hybRange.Z5.min +
               (_hybRange.Z5.max - _hybRange.Z5.min) * gsl_rng_uniform(rng);
      hyb.Z7 = _hybRange.Z7.min +
               (_hybRange.Z7.max - _hybRange.Z7.min) * gsl_rng_uniform(rng);

      if (!thdm.set_param_hybrid(hyb))
        continue;
      
      aU = aURange.min + (aURange.max - aURange.min) * gsl_rng_uniform(rng);
      aD = aDRange.min + (aDRange.max - aDRange.min) * gsl_rng_uniform(rng);
      aL = 1. / hyb.tanb;

      thdm.set_yukawa_aligned( aU, aD, aL);

      // thdm.set_logLevel(LOG_WARNINGS); //DEBUG

      if (parameter_point_is_okay(thdm))
      {
        break;
      }
    }
    break;
  }
  case FLAT_Z2_CP_CONSERVED:
  {
    /**
     * @brief FLAT_Z2_CP_CONSERVED
     * 
     * Flat dummy scan for debugging.
     */

    thdm.reset_to_saved_state();

    Base_generic gen;

    // Generates a random parameter point that is perturbative, unitary and
    // stable.
    for (;;)
    {
      gen.generate_random_softCpConserved( rng);

      if (!thdm.set_param_gen(gen))
        continue;

      thdm.set_yukawa_type(_hybRange.yukawaType);

      // thdm.set_logLevel(LOG_WARNINGS); //DEBUG

      if (parameter_point_is_okay(thdm))
      {
        break;
      }
    }
    break;
  }
  default:
  {
    cout << "[ERROR]: No scan chosen.\n";
    break;
  }
  }
}

bool ScanSystem::parameter_point_is_okay(THDM_complex &thdm) const
{
  if (!thdm.is_cp_conserved())
    return false;
  if (!thdm.is_pert_unit_stab())
    return false;

  if (_runSpheno)
  {
    thdm.run_spheno(1);
    if (!thdm.is_within_spheno_limits())
    {
      // cout << "Parameter point excluded by SPheno.\n"; // DEBUG
      return false;
    }
  }
  else
  {
    // double mh = thdm.get_higgs_treeLvl_masses()[0];
    // if (std::abs(mh - 125.) > 5.)
    //   return false;
  }

#if defined HiggsBounds
  thdm.run_higgsBoundsSignals();
  if (!thdm.is_allowed_by_HBHS())
  {
    cout << "Parameter point excluded by HBHS.\n";
    return false;
  }
#endif
  // cout << "Parameter point is okay!\n"; // DEBUG
  return true;
}

bool ScanSystem::satisfies_requirements(const THDM_complex &thdm) const
{
  // cout << "(minEnergy = " << _minEnergy << " GeV), Reached scale " <<
  // thdm.get_rgeResults().ef << " GeV\n";

  static const double COMPARISON_PRECISION = 0.99;
  if (thdm.get_rgeResults().ef / _minEnergy >= COMPARISON_PRECISION)
  {
    return true;
  }
  else
    return false;
}

void ScanSystem::print_status() const
{
  cout << "\nScanning...\n";
  print_time();
  print_scanType();
  cout << "So far, found " << _pointsFound << " good parameter points.\n";
  if (_pointsFound > 0)
    print_average_point_time();

  cout << "\n";
}

void ScanSystem::print_scanType() const
{
  switch (_scanType)
  {
  case EXACT_Z2:
  {
    cout << "Scan type: EXACT_Z2_ALIGNED\n";
    break;
  }
  case SOFTLY_BROKEN_Z2:
  {
    cout << "Scan type: SOFTLY_BROKEN_Z2\n";
    break;
  }
  case HARD_BROKEN_Z2:
  {
    cout << "Scan type: HARD_BROKEN_Z2\n";
    break;
  }
  case MODEL_PROCESS:
  {
    cout << "Scan type: MODEL_PROCESS\n";
    break;
  }
  case ALIGNED_YUKAWA:
  {
    cout << "Scan type: YUKAWA_ALIGNED, ";
    switch (_fermionSector)
    {
    case UP:
      cout << "UP sector\n";
      break;
    case DOWN:
      cout << "DOWN sector\n";
      break;
    case LEPTON:
      cout << "LEPTON sector\n";
      break;
    default:
      cout << "Unknown sector\n";
      break;
    }
    break;
  }
  case SOFTLY_BROKEN_Z2_ALIGNED:
  {
    cout << "Scan type: SOFTLY_BROKEN_Z2_ALIGNED\n";
    break;
  }
  case FLAT_Z2_CP_CONSERVED:
  {
    cout << "Scan type: FLAT_Z2_CP_CONSERVED\n";
    break;
  }
  default:
  {
    cout << "Scan type: UNKNOWN\n";
    break;
  }
  }
}

void ScanSystem::print_yukawaType() const
{
  switch (_hybRange.yukawaType)
  {
  case NO_SYMMETRY:
    cout << "Z_2 Yukawa symmetry: NONE\n";
    break;
  case TYPE_I:
    cout << "Z_2 Yukawa symmetry: TYPE_I\n";
    break;
  case TYPE_II:
    cout << "Z_2 Yukawa symmetry: TYPE_II\n";
    break;
  case TYPE_III:
    cout << "Z_2 Yukawa symmetry: TYPE_III\n";
    break;
  case TYPE_IV:
    cout << "Z_2 Yukawa symmetry: TYPE_IV\n";
    break;
  }
}

void ScanSystem::print_to_files(const THDM_complex &thdm,
                                const THDM_complex &thdmEvolved,
                                const int threadID)
{

  // Create a 1-loop evolved thdm
  THDM_complex thdmEvolved_1loop(thdm);
  RgeConfig rgeConfig = thdm.get_rgeConfig();
  rgeConfig.twoloop = false;
  thdmEvolved_1loop.set_rgeConfig(rgeConfig);
  thdmEvolved_1loop.evolve();

  std::unique_lock<std::mutex> lck(_fileMutex);

  if (_pointsFound < _maxPoints)
  {
    cout << "Thread " << threadID + 1
              << " is writing to files! Pointsfound = " << ++_pointsFound
              << std::endl;

    print_evolution_data(thdmEvolved_1loop, _scanDataFile_1loop);
    print_evolution_data(thdmEvolved, _scanDataFile);

    print_evolved_data(thdmEvolved_1loop, _scanDataFile_1loop_evolved);
    print_evolved_data(thdmEvolved, _scanDataFile_evolved);

    print_generic_basis( thdmEvolved_1loop, _scanDataFile_generic_1loop_evolved);
    print_generic_basis( thdmEvolved, _scanDataFile_generic_evolved);

    print_generic_basis( thdm, _scanDataFile_generic);
    print_physical_data_files(thdm);

    if (_scanType == ALIGNED_YUKAWA || _scanType == SOFTLY_BROKEN_Z2_ALIGNED)
    {
      vector<complex<double>> aFVec = thdm.get_aF();
      vector<double> real_aFVec = {real(aFVec[0]), imag(aFVec[0]),
                                        real(aFVec[1]), imag(aFVec[1]),
                                        real(aFVec[2]), imag(aFVec[2])};
      _files.open_file(_scanDataFile_yukawa, true);
      _files.add_line(_scanDataFile_yukawa, real_aFVec);
    }
    if (_runSpheno)
    {
      _files.open_file(_scanDataFile_spheno, true);
      _files.add_line(_scanDataFile_spheno, thdm.get_spheno_output());
    }
    _files.close_streams();
  }
  else
    _quitScan = true;
}

void ScanSystem::print_evolution_data(const THDM_complex &thdm,
                                      const string &file)
{
  _files.open_file(file, true);
  RgeResults rgeResults = thdm.get_rgeResults();

  // Calculating minimum energy where pert, unit or stab is violated
  double efViolation = rgeResults.ef;
  if (rgeResults.ef_pert < efViolation && rgeResults.ef_pert != -1)
    efViolation = rgeResults.ef_pert;
  if (rgeResults.ef_unit < efViolation && rgeResults.ef_unit != -1)
    efViolation = rgeResults.ef_unit;
  if (rgeResults.ef_stab < efViolation && rgeResults.ef_stab != -1)
    efViolation = rgeResults.ef_stab;

  _files.add_line(file, vector<double>{rgeResults.ef, rgeResults.ef_pert,
                                            rgeResults.ef_unit,
                                            rgeResults.ef_stab, efViolation});
}

void ScanSystem::print_evolved_data(const THDM_complex &thdm,
                                    const string &file)
{
  _files.open_file(file, true);

  double lamD23 = thdm.get_lamF_element(DOWN, 1, 2);

  auto maxYuk = thdm.get_largest_nonDiagonal_rF();
  auto maxYuk_chengSher = thdm.get_largest_nonDiagonal_lamF();
  auto maxLam = thdm.get_largest_lambda();
  complex<double> z2Quantity = thdm.get_z2_breaking_quantity();

  _files.add_line(
      file,
      vector<string>{
          stringAuto(thdm.get_renormalization_scale()), stringAuto(abs(z2Quantity)),
          stringAuto(real(z2Quantity)), stringAuto(imag(z2Quantity)),
          std::get<0>(maxYuk), stringAuto(abs(std::get<3>(maxYuk))),
          std::get<0>(maxYuk_chengSher),
          stringAuto(abs(std::get<3>(maxYuk_chengSher))), std::get<0>(maxLam),
          stringAuto(std::get<2>(maxLam)), stringAuto(lamD23)});
}

void ScanSystem::print_generic_basis(const THDM_complex &thdm,
                                     const string &file)
{
  Base_generic gen = thdm.get_param_gen();

  _files.open_file( file, true);
  _files.add_line( file, gen.convert_to_vector());
}

void ScanSystem::print_physical_data_files(const THDM_complex &thdm)
{
  vector<double> masses = thdm.get_higgs_treeLvl_masses();

  _files.open_file(_scanDataFile_higgs, true);
  _files.open_file(_scanDataFile_invariant, true);
  _files.open_file(_scanDataFile_physical, true);

  Base_higgs higgs = thdm.get_param_higgs();
  Base_invariant inv = thdm.get_param_invariant();

  _files.add_line(_scanDataFile_higgs, higgs.convert_to_vector());
  _files.add_line(_scanDataFile_invariant, inv.convert_to_vector());

  // Calculate the physical parameters mA, mh, mH, cba and sba
  double v2 = thdm.get_v2();
  double mA =
      std::sqrt(inv.mHc * inv.mHc + 0.5 * (higgs.Z4 - real(higgs.Z5)) * v2);

  double mh = std::sqrt(
      0.5 * (mA * mA + (higgs.Z1 + real(higgs.Z5)) * v2 -
             std::sqrt((mA * mA + (real(higgs.Z5) - higgs.Z1) * v2) *
                           (mA * mA + (real(higgs.Z5) - higgs.Z1) * v2) +
                       4. * real(higgs.Z6) * real(higgs.Z6) * v2 * v2)));

  double mH = std::sqrt(
      0.5 * (mA * mA + (higgs.Z1 + real(higgs.Z5)) * v2 +
             std::sqrt((mA * mA + (real(higgs.Z5) - higgs.Z1) * v2) *
                           (mA * mA + (real(higgs.Z5) - higgs.Z1) * v2) +
                       4. * real(higgs.Z6) * real(higgs.Z6) * v2 * v2)));

  double cba = -real(higgs.Z6) * v2 /
               std::sqrt((mH * mH - mh * mh) * (mH * mH - higgs.Z1 * v2));

  double sba = std::abs(higgs.Z6) * v2 /
               std::sqrt((mH * mH - mh * mh) * (higgs.Z1 * v2 - mh * mh));

  _files.add_line(_scanDataFile_physical,
                  vector<double>{mh, mH, mA, cba, sba, sqrt(v2)});
}
void ScanSystem::create_model_file(const THDM_complex &thdm) const
{
  static std::atomic<int> numModels(0);

  thdm.write_slha_file(-1, "output/" + _directory + "/modelFiles/thdm_" +
                               std::to_string(++numModels) + ".txt");
}

vector<THDM_complex>
ScanSystem::load_model_files(const string &dir) const
{
  vector<THDM_complex> thdmVec;

  int id = 1;
  for (;;)
  {
    thdmVec.emplace_back();
    thdmVec.back().set_logLevel(LOG_ERRORS);
    if (!thdmVec.back().set_from_slha_file(dir + "/thdm_" +
                                           std::to_string(id++) + ".txt"))
    {
      thdmVec.pop_back();
      break;
    }
  }
  return thdmVec;
}

void ScanSystem::create_sm_modelFile(const SM_RGE &sm)
{
  std::unique_lock<std::mutex> lck(_sm_modelFileMutex);

  _files.create_model_file(sm, "sm_modelFile");
}

gsl_rng *ScanSystem::create_gsl_rng(const int seedNumber) const
{
  // Random number generator
  gsl_rng *rng = gsl_rng_alloc(gsl_rng_taus2);
  int seed = time(0) + seedNumber * 3715291;
  gsl_rng_set(rng, seed); // Seed the random number generator
  cout << "New gsl_rng with seed = " << seed << std::endl;
  return rng;
}

void ScanSystem::create_parameter_info_file() const
{
  /* Creating scan info file */
  Table::change_colorMode(false);

  std::streambuf *psbuf, *backup;
  std::ostringstream stringStream(std::ostringstream::ate);

  backup = cout.rdbuf(); // back up cout's streambuf

  psbuf = stringStream.rdbuf(); // get file's streambuf
  cout.rdbuf(psbuf);       // assign streambuf to cout

  cout << "###############################################################"
               "######\n"
            << "# 2HDM ScanSystem info file\n"
            << "# Created: " << date() << "\n"
            << "###############################################################"
               "######\n\n";

  cout << _description << std::endl;

  print_scanType();
  if (_scanType != ALIGNED_YUKAWA)
  {
    _hybRange.print();
    print_yukawaType();
  }

  cout << "Performed scan using " << _threads << " threads.\n"
            << "Maximum parameter points: " << _maxPoints << std::endl
            << "Minimum energy constraint: " << _minEnergy << std::endl;
  if (_runSpheno)
  {
    cout << "SPheno output: Yes\n"
              << "MassCorrections loopLvl: " << _massLoopLvl << std::endl;
  }
  else
    cout << "SPheno output: No\n";

  _rgeConfig.print();

  _files.create_single_file("scanInfo.txt", stringStream.str());

  cout.rdbuf(backup); // restore cout's original streambuf
  Table::change_colorMode(true);
}
