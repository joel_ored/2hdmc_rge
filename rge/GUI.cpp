#include "GUI.h"
// #include "SM.h"
// #include "Structures.h"
// #include "ParameterScan.h"
// #include "CalcGradient.h"
#include "HelpFunctions.h"

#include <gtkmm.h>
#include <gtkmm/builder.h>
#include <complex>
#include <vector>
#include <stdexcept>
#include <string>
#include <iostream>

GUI::GUI() : _grid(nullptr),
             // _scan(nullptr),
             _kU("kU"),
             _kD("kD"),
             _kL("kL"),
             _rU("rU"),
             _rD("rD"),
             _rL("rL"),
             _eta1U("eta1U"),
             _eta1D("eta1D"),
             _eta1L("eta1L"),
             _eta2U("eta2U"),
             _eta2D("eta2D"),
             _eta2L("eta2L"),
             _fileSystem("output/"),
             _currentBasis(BaseType::HIGGS)
{
    /** The user interface is buildt as Gtk::Grid in glade3 and saved in
     * a file GUI.glade. All Gtk objects pointers are retrieved with the
     * help of Gtk::Builder. All links to functions are written below.
     **/

    /* Creates the builder. */
    _builder = Gtk::Builder::create_from_file("src/Glade/GUI.glade");

    /* grid1 is a 2 by 2 grid that are subdivided further. */
    _builder->get_widget("grid1", _grid);

    /* Initialization of all the buttons. */
    _builder->get_widget("buttonClose", _buttonClose);
    _buttonClose->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_close));

    _builder->get_widget("buttonPrintAll", _buttonPrintAll);
    _buttonPrintAll->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_printAll));

    _builder->get_widget("buttonPrintPotential", _buttonPrintPotential);
    _buttonPrintPotential->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_printPotential));

    _builder->get_widget("buttonEvolve", _buttonEvolve);
    _buttonEvolve->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_evolve));

    _builder->get_widget("buttonSet", _buttonSet);
    _buttonSet->signal_clicked().connect(sigc::mem_fun(*this, &GUI::updateThdmFromGui));

    _builder->get_widget("buttonLoad", _buttonLoad);
    _buttonLoad->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_load));

    _builder->get_widget("buttonGradient", _buttonCalcGradient);
    _buttonCalcGradient->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_calcGradient));

    _builder->get_widget("buttonFindMax", _buttonFindMaximum);
    _buttonFindMaximum->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_findMax));

    _builder->get_widget("buttonLambdaplot", _buttonPlotLambda);
    _buttonPlotLambda->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_plot_lambda));

    _builder->get_widget("buttonMijplot", _buttonPlotMij);
    _buttonPlotMij->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_plot_Mij));

    _builder->get_widget("buttonTanbPlot", _buttonPlotTanb);
    _buttonPlotTanb->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_plot_tanb));

    _builder->get_widget("buttonMassesPlot", _buttonPlotMasses);
    _buttonPlotMasses->signal_clicked().connect(sigc::mem_fun(*this, &GUI::button_plot_masses));

    _buttonsPlotYukawa.resize(12, nullptr);
    for (int i = 0; i < 12; i++)
    {
        _builder->get_widget("button_" + _matrixNames[i], _buttonsPlotYukawa[i]);
        _buttonsPlotYukawa[i]->signal_clicked().connect(sigc::bind<int>(sigc::mem_fun(*this, &GUI::button_plot_yukawa), i));
    }

    _builder->get_widget("PotentialNotebook", _notebookPotential);
    _builder->get_widget("YukawaNotebook", _notebookYukawa);
    _builder->get_widget("comboBoxBasis", _comboBoxBase);
    _comboBoxBase->signal_changed().connect(sigc::mem_fun(*this, &GUI::change_basis));

    _builder->get_widget("comboboxZ2symmetry", _comboBoxZ2symmetry);
    _comboBoxZ2symmetry->signal_changed().connect(sigc::mem_fun(*this, &GUI::updateThdmFromGui));

    _builder->get_widget("evolutionName", _entryEvolutionName);
    _builder->get_widget("dataOutput", _buttonDataOutput);
    _builder->get_widget("consolOutput", _buttonConsoleOutput);
    _builder->get_widget("twoLoop", _buttonTwoLoop);
    _builder->get_widget("pertCheck", _buttonPerturbativity);
    _builder->get_widget("unitCheck", _buttonUnitarity);
    _builder->get_widget("stabCheck", _buttonStability);
    _builder->get_widget("odeSteps", _buttonOdeSteps);

    initializeSpinbuttons();

    /* Assigns matrices. */
    _kU.setEntries(_builder);
    _kD.setEntries(_builder);
    _kL.setEntries(_builder);
    _rU.setEntries(_builder);
    _rD.setEntries(_builder);
    _rL.setEntries(_builder);

    _eta1U.setEntries(_builder);
    _eta1D.setEntries(_builder);
    _eta1L.setEntries(_builder);
    _eta2U.setEntries(_builder);
    _eta2D.setEntries(_builder);
    _eta2L.setEntries(_builder);

    /* Adds the _grid to the window. */
    add(*_grid);

    /* Further options for the Gtk::Window. */
    set_title("THDMC GUI");
    set_position(Gtk::WIN_POS_CENTER);
    show_all_children();

    /* Sets the _thdm parameters from the spinbuttons. */
    init_THDM();
}

GUI::~GUI()
{
}

void GUI::initializeSpinbuttons()
{
    _spinbuttons.resize(33);
    _builder->get_widget("spinbutton_mHc", _spinbuttons[0]);
    _builder->get_widget("spinbutton_Z1", _spinbuttons[1]);
    _builder->get_widget("spinbutton_Z2", _spinbuttons[2]);
    _builder->get_widget("spinbutton_Z3", _spinbuttons[3]);
    _builder->get_widget("spinbutton_Z4", _spinbuttons[4]);
    _builder->get_widget("spinbutton_reZ5", _spinbuttons[5]);
    _builder->get_widget("spinbutton_imZ5", _spinbuttons[6]);
    _builder->get_widget("spinbutton_reZ6", _spinbuttons[7]);
    _builder->get_widget("spinbutton_imZ6", _spinbuttons[8]);
    _builder->get_widget("spinbutton_reZ7", _spinbuttons[9]);
    _builder->get_widget("spinbutton_imZ7", _spinbuttons[10]);

    _builder->get_widget("spinbutton_tanb", _spinbuttons[11]);

    _builder->get_widget("spinbutton_mh1", _spinbuttons[12]);
    _builder->get_widget("spinbutton_mh2", _spinbuttons[13]);
    _builder->get_widget("spinbutton_mh3", _spinbuttons[14]);
    _builder->get_widget("spinbutton_mHc_hyb", _spinbuttons[15]);
    _builder->get_widget("spinbutton_s12", _spinbuttons[16]);
    _builder->get_widget("spinbutton_c13", _spinbuttons[17]);
    _builder->get_widget("spinbutton_Z2_hyb", _spinbuttons[18]);
    _builder->get_widget("spinbutton_Z3_hyb", _spinbuttons[19]);
    _builder->get_widget("spinbutton_reZ7_hyb", _spinbuttons[20]);
    _builder->get_widget("spinbutton_imZ7_hyb", _spinbuttons[21]);

    _builder->get_widget("spinbutton_m12", _spinbuttons[22]);
    _builder->get_widget("spinbutton_lambda1", _spinbuttons[23]);
    _builder->get_widget("spinbutton_lambda2", _spinbuttons[24]);
    _builder->get_widget("spinbutton_lambda3", _spinbuttons[25]);
    _builder->get_widget("spinbutton_lambda4", _spinbuttons[26]);
    _builder->get_widget("spinbutton_relambda5", _spinbuttons[27]);
    _builder->get_widget("spinbutton_imlambda5", _spinbuttons[28]);
    _builder->get_widget("spinbutton_relambda6", _spinbuttons[29]);
    _builder->get_widget("spinbutton_imlambda6", _spinbuttons[30]);
    _builder->get_widget("spinbutton_relambda7", _spinbuttons[31]);
    _builder->get_widget("spinbutton_imlambda7", _spinbuttons[32]);
}

void GUI::init_THDM()
{
    read_RGE_options();
    updateThdmFromGui();
}

void GUI::button_printAll()
{
    /* Prints to the consol. */
    _thdm.print_all();
}

void GUI::button_printPotential()
{
    /* Prints to the consol. */
    _thdm.print_potential();
    _thdm.print_higgs_masses();
}

void GUI::read_RGE_options()
{
    // RGE evolution options
    _rgeConfig.finalEnergyScale = 1.E18; // End of running scale
    _rgeConfig.evolutionName = _entryEvolutionName->get_text();
    _rgeConfig.dataOutput = _buttonDataOutput->get_active();
    _rgeConfig.consoleOutput = _buttonConsoleOutput->get_active();
    _rgeConfig.twoloop = _buttonTwoLoop->get_active();
    _rgeConfig.perturbativity = _buttonPerturbativity->get_active();
    _rgeConfig.unitarity = _buttonUnitarity->get_active();
    _rgeConfig.stability = _buttonStability->get_active();
    _rgeConfig.steps = (int)_buttonOdeSteps->get_value();

    _thdm.set_rgeConfig(_rgeConfig);
}
void GUI::button_evolve()
{
    Timer timer;
    /* Reads in the parameters and RGE options from the buttons. */
    updateThdmFromGui();
    read_RGE_options();

    // _thdm.RGE_evolve();
    _thdm.evolve();
}

void GUI::button_load()
{
    GtkWidget *dialog;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
    gint res;

    dialog = gtk_file_chooser_dialog_new("Open modelfile, thdm.txt",
                                         this->gobj(),
                                         action,
                                         "Cancel",
                                         GTK_RESPONSE_CANCEL,
                                         "Open",
                                         GTK_RESPONSE_ACCEPT,
                                         NULL);

    res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT)
    {
        char *fileName;
        GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);
        fileName = gtk_file_chooser_get_filename(chooser);
        _fileSystem.load_model_file(_thdm, fileName);
        g_free(fileName);
    }

    gtk_widget_destroy(dialog);

    updateGuiFromThdm();
}

void GUI::updateThdmFromGui()
{
    _thdm.reset();
    Base_higgs higgs;
    Base_generic gen;
    Base_invariant inv;

    /* The basis of choice is set by the _comboBoxBase (HIGGS; GENERIC or HYBRID). */
    switch (_currentBasis)
    {
    case HIGGS:
        higgs.mHc = _spinbuttons[0]->get_value();
        higgs.Z1 = _spinbuttons[1]->get_value();
        higgs.Z2 = _spinbuttons[2]->get_value();
        higgs.Z3 = _spinbuttons[3]->get_value();
        higgs.Z4 = _spinbuttons[4]->get_value();
        higgs.Z5 = std::complex<double> (_spinbuttons[5]->get_value(), _spinbuttons[6]->get_value());
        higgs.Z6 = std::complex<double> (_spinbuttons[7]->get_value(), _spinbuttons[8]->get_value());
        higgs.Z7 = std::complex<double> (_spinbuttons[9]->get_value(), _spinbuttons[10]->get_value());
        higgs.beta = atan(_spinbuttons[11]->get_value());

        _thdm.set_param_higgs(higgs);
        break;
    case GENERIC:
        gen.M12 = std::complex<double>(pow(_spinbuttons[22]->get_value(), 2), 0.);
        gen.Lambda1 = _spinbuttons[23]->get_value();
        gen.Lambda2 = _spinbuttons[24]->get_value();
        gen.Lambda3 = _spinbuttons[25]->get_value();
        gen.Lambda4 = _spinbuttons[26]->get_value();
        gen.Lambda5 = std::complex<double> ( _spinbuttons[27]->get_value(), _spinbuttons[28]->get_value());
        gen.Lambda6 = std::complex<double> ( _spinbuttons[29]->get_value(), _spinbuttons[30]->get_value());
        gen.Lambda7 = std::complex<double> ( _spinbuttons[31]->get_value(), _spinbuttons[32]->get_value());
        gen.beta = atan(_spinbuttons[11]->get_value());

        _thdm.set_param_gen(gen);
        break;
    case INVARIANT:
        inv.mh[0] = _spinbuttons[12]->get_value();
        inv.mh[1] = _spinbuttons[13]->get_value();
        inv.mh[2] = _spinbuttons[14]->get_value();
        inv.mHc = _spinbuttons[15]->get_value();
        inv.s12 = _spinbuttons[16]->get_value();
        inv.c13 = _spinbuttons[17]->get_value();
        inv.Z2 = _spinbuttons[18]->get_value();
        inv.Z3 = _spinbuttons[19]->get_value();
        inv.Z7inv = std::complex<double> (_spinbuttons[20]->get_value(), _spinbuttons[21]->get_value());

        _thdm.set_param_invariant(inv);
        break;
    default:
        std::cout << "[ERROR]: No basis is chosen.\n";
        break;
    }

    _thdm.set_tanb(_spinbuttons[11]->get_value());

    /* Switch depending on Z_2 symmetry set. */
    switch (_comboBoxZ2symmetry->get_active_row_number())
    {
    case 0:
        _thdm.set_yukawa_type(NO_SYMMETRY);

        /* Switch depending on active basis. */
        switch ( _currentBasis)
        {
        case HIGGS:
            _thdm.set_yukawa_manual(_rU.read(), _rD.read(), _rL.read());
            break;
        case GENERIC:
            _thdm.set_yukawa_eta(_eta1U.read(), _eta1D.read(), _eta1L.read(), _eta2U.read(), _eta2D.read(), _eta2L.read());
            break;
        case HYBRID:
            _thdm.set_yukawa_manual(_rU.read(), _rD.read(), _rL.read());
            break;
        default:
            std::cout << "[ERROR]: No basis is chosen.\n";
            break;
        }
        break;
    case 1:
        _thdm.set_yukawa_type(TYPE_I);
        break;
    case 2:
        _thdm.set_yukawa_type(TYPE_II);
        break;
    case 3:
        _thdm.set_yukawa_type(TYPE_III);
        break;
    case 4:
        _thdm.set_yukawa_type(TYPE_IV);
        break;
    default:
        break;
    }
    /* Updates the matrix entries. */
    higgs = _thdm.get_param_higgs();
    _kU.set(higgs.kU);
    _kD.set(higgs.kD);
    _kL.set(higgs.kL);
    _rU.set(higgs.rU);
    _rD.set(higgs.rD);
    _rL.set(higgs.rL);
    gen = _thdm.get_param_gen();
    _eta1U.set(gen.eta1U);
    _eta1D.set(gen.eta1D);
    _eta1L.set(gen.eta1L);
    _eta2U.set(gen.eta2U);
    _eta2D.set(gen.eta2D);
    _eta2L.set(gen.eta2L);
}

void GUI::updateGuiFromThdm()
{
    /* Updates the matrix entries. */
    Base_higgs higgs = _thdm.get_param_higgs();
    _kU.set(higgs.kU);
    _kD.set(higgs.kD);
    _kL.set(higgs.kL);
    _rU.set(higgs.rU);
    _rD.set(higgs.rD);
    _rL.set(higgs.rL);
    Base_generic gen = _thdm.get_param_gen();
    _eta1U.set(gen.eta1U);
    _eta1D.set(gen.eta1D);
    _eta1L.set(gen.eta1L);
    _eta2U.set(gen.eta2U);
    _eta2D.set(gen.eta2D);
    _eta2L.set(gen.eta2L);
    Base_invariant inv = _thdm.get_param_invariant();

    _comboBoxZ2symmetry->set_active((int)_thdm.get_yukawa_type());

    _spinbuttons[0]->set_value(higgs.mHc);
    _spinbuttons[1]->set_value(higgs.Z1);
    _spinbuttons[2]->set_value(higgs.Z2);
    _spinbuttons[3]->set_value(higgs.Z3);
    _spinbuttons[4]->set_value(higgs.Z4);
    _spinbuttons[5]->set_value(real(higgs.Z5));
    _spinbuttons[6]->set_value(imag(higgs.Z5));
    _spinbuttons[7]->set_value(real(higgs.Z6));
    _spinbuttons[8]->set_value(imag(higgs.Z6));
    _spinbuttons[9]->set_value(real(higgs.Z7));
    _spinbuttons[10]->set_value(imag(higgs.Z7));

    _spinbuttons[11]->set_value(tan(gen.beta));

    _spinbuttons[22]->set_value(sqrt(abs(gen.M12)));
    _spinbuttons[23]->set_value(gen.Lambda1);
    _spinbuttons[24]->set_value(gen.Lambda2);
    _spinbuttons[25]->set_value(gen.Lambda3);
    _spinbuttons[26]->set_value(gen.Lambda4);
    _spinbuttons[27]->set_value(real(gen.Lambda5));
    _spinbuttons[28]->set_value(imag(gen.Lambda5));
    _spinbuttons[29]->set_value(real(gen.Lambda6));
    _spinbuttons[30]->set_value(imag(gen.Lambda6));
    _spinbuttons[31]->set_value(real(gen.Lambda7)); 
    _spinbuttons[32]->set_value(imag(gen.Lambda7));

    _spinbuttons[12]->set_value(inv.mh[0]);
    _spinbuttons[13]->set_value(inv.mh[1]);
    _spinbuttons[14]->set_value(inv.mh[2]);
    _spinbuttons[15]->set_value(inv.mHc);
    _spinbuttons[16]->set_value(inv.s12);
    _spinbuttons[17]->set_value(inv.c13);
    _spinbuttons[18]->set_value(inv.Z2);
    _spinbuttons[19]->set_value(inv.Z3);
    _spinbuttons[20]->set_value(real(inv.Z7inv));
    _spinbuttons[21]->set_value(imag(inv.Z7inv));
}

void GUI::button_plot_yukawa(const int matrixID) const
{
    std::vector<std::string> _matrixNames = {"kU", "kD", "kL", "rU", "rD", "rL", "eta1U", "eta1D", "eta1L", "eta2U", "eta2D", "eta2L"};
    std::string sysCommand = "gnuplot --persist output/" + _rgeConfig.evolutionName + "/" + _matrixNames[matrixID] + ".gnu";
    system(sysCommand.c_str());
}

void GUI::button_calcGradient()
{
    Timer timer;
    /* Reads in the parameters and RGE options from the buttons. */
    updateThdmFromGui();
    read_RGE_options();
    // TODO
    // // _gradient = calcGradient(_thdm);
    // _gradient = _rgeSystem.gradient(_thdm);
    // _gradient.print();
}

void GUI::button_findMax()
{
    /* Reads in the parameters and RGE options from the buttons. */
    updateThdmFromGui();
    read_RGE_options();

    // TODO
    /* Creates a new thread which runs a ParameterScan that walks
    along the gradient. If the button that toggles the function is called 
    again, it quits the ParameterScan and joins the additional thread. */
    // if (_buttonFindMaximum->get_active())
    //     _thread = std::thread([this] { this->runAlongGradient(); });
    // else
    // {
    //     // ParameterScan::Quit();
    //     if (_thread.joinable())
    //         _thread.join();
    // }
}

void GUI::button_plot_lambda() const
{
    std::string sysCommand = "gnuplot --persist output/" + _rgeConfig.evolutionName + "/lambda.gnu";
    system(sysCommand.c_str());
}

void GUI::button_plot_Mij() const
{
    std::string sysCommand = "gnuplot --persist output/" + _rgeConfig.evolutionName + "/Mij.gnu";
    system(sysCommand.c_str());
}

void GUI::button_plot_tanb() const
{
    std::string sysCommand = "gnuplot --persist output/" + _rgeConfig.evolutionName + "/angles.gnu";
    system(sysCommand.c_str());
}

void GUI::button_plot_masses() const
{
    std::string sysCommand = "gnuplot --persist output/" + _rgeConfig.evolutionName + "/masses.gnu";
    system(sysCommand.c_str());
}

void GUI::button_close()
{
    hide();
}

void GUI::runAlongGradient()
{
    // TODO
    // /* A parameter scan object that can scan a 2HDM.
    //    Stores output files in the directory given in
    //    the constructor. */
    // _scan = new ParameterScan("test", 2);
    // RGE_config options = _options;
    // options.consolOutput = false;
    // options.dataOutput = false;
    // _scan->set_RGE_config(options);

    // // Scanning, starting from a CP conserved 2HDM at the EW.
    // Base_generic genFound =_scan->findMax(_thdm.get_param_gen());
    // Base_higgs higgs;
    // _thdm.reset();
    // _thdm.set_param_gen(genFound);

    // /* When the scan is turned off, the _thdm is set to the
    // last parameter point. The basis choice is determined by
    // the _comboBoxBase. */
    // switch( _comboBoxBase->get_active_row_number() ){
    //     case 0:
    //         higgs = _thdm.get_param_higgs();
    //         _spinbuttons[0]->set_value( higgs.mHc );
    //         _spinbuttons[1]->set_value( higgs.Z1 );
    //         _spinbuttons[2]->set_value( higgs.Z2 );
    //         _spinbuttons[3]->set_value( higgs.Z3 );
    //         _spinbuttons[4]->set_value( higgs.Z4 );
    //         _spinbuttons[5]->set_value( abs(higgs.Z5) );
    //         _spinbuttons[7]->set_value( abs(higgs.Z6) );
    //         _spinbuttons[9]->set_value( abs(higgs.Z7) );
    //         _spinbuttons[6]->set_value( arg(higgs.Z5) );
    //         _spinbuttons[8]->set_value( arg(higgs.Z6) );
    //         _spinbuttons[10]->set_value( arg(higgs.Z7) );
    //         break;
    //     case 1:
    //         _spinbuttons[0]->set_value( abs(genFound.M12) );
    //         _spinbuttons[1]->set_value( genFound.Lambda1 );
    //         _spinbuttons[2]->set_value( genFound.Lambda2 );
    //         _spinbuttons[3]->set_value( genFound.Lambda3 );
    //         _spinbuttons[4]->set_value( genFound.Lambda4 );
    //         _spinbuttons[5]->set_value( abs(genFound.Lambda5) );
    //         _spinbuttons[7]->set_value( abs(genFound.Lambda6) );
    //         _spinbuttons[9]->set_value( abs(genFound.Lambda7) );
    //         _spinbuttons[6]->set_value( arg(genFound.Lambda5) );
    //         _spinbuttons[8]->set_value( arg(genFound.Lambda6) );
    //         _spinbuttons[10]->set_value( arg(genFound.Lambda7) );
    //         _spinbuttons[11]->set_value( tan(genFound.beta) );
    //         break;
    //     default:
    //         std::cout << "[ERROR]: No basis is chosen.\n";
    //         break;
    // }

    // delete _scan;
}

void GUI::change_basis()
{
    _thdm.print_potential();
    std::cout << "updateThdmFromGui()\n";
    // Sets the _thdm to the current settings of the GUI
    updateThdmFromGui();
    _thdm.print_potential();
    std::cout << "updateGuiFromThdm()\n";
    // Updates the parameter entries to _thdm's values.
    updateGuiFromThdm();

    /* Switches between what parameters are being visible for editing. */
    switch (_comboBoxBase->get_active_row_number())
    {
    case 0:
        _notebookPotential->set_current_page(0);
        _notebookYukawa->set_current_page(0);
        _currentBasis = HIGGS;
        break;
    case 1:
        _notebookPotential->set_current_page(1);
        _notebookYukawa->set_current_page(1);
        _currentBasis = GENERIC;
        break;
    case 2:
        _notebookPotential->set_current_page(2);
        _notebookYukawa->set_current_page(0);
        _currentBasis = HYBRID;
        break;
    }
}

/*-----------------MatrixGUI--------------------*/

MatrixGUI::MatrixGUI(const std::string &name) : _name(name)
{
    _elements.resize(9, nullptr); // Matrix elements
    _matrix.setZero();

    /* The std::stod function that turns strings to doubles,
     may use "," as seperator. This command enforces "." as
     seperator. */
    std::setlocale(LC_ALL, "C");
}
MatrixGUI::~MatrixGUI()
{
}

void MatrixGUI::setEntries(Glib::RefPtr<Gtk::Builder> builder)
{
    builder->get_widget(_name + "_11", _elements[0]);
    builder->get_widget(_name + "_12", _elements[1]);
    builder->get_widget(_name + "_13", _elements[2]);
    builder->get_widget(_name + "_21", _elements[3]);
    builder->get_widget(_name + "_22", _elements[4]);
    builder->get_widget(_name + "_23", _elements[5]);
    builder->get_widget(_name + "_31", _elements[6]);
    builder->get_widget(_name + "_32", _elements[7]);
    builder->get_widget(_name + "_33", _elements[8]);
}

Eigen::Matrix3cd MatrixGUI::read()
{
    int index = 0;
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
        {
            _matrix(i, j) = entryToComplex(_elements[index]->get_text());
            index++;
        }
    update();
    return _matrix;
}

void MatrixGUI::set(const Eigen::Matrix3cd &matrix)
{
    _matrix = matrix;
    update();
}

void MatrixGUI::update()
{
    int index = 0;
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
        {
            _elements[index]->set_text("( " + stringAuto(real(_matrix(i, j))) + ", " + stringAuto(imag(_matrix(i, j))) + ")");
            index++;
        }
}

std::complex<double> MatrixGUI::entryToComplex(Glib::ustring entry) const
{
    std::string entryString = entry;
    std::string entryReal, entryImag;

    int entryLength = entryString.length();
    int commaPosition = 0;
    int endPosition = 0;
    int commas = 0;
    int ends = 0;
    std::string comma = ",";
    std::string end = ")";

    if (entryLength > 3)
    {
        for (int i = 0; i < entryLength; i++)
        {
            if (entryString.at(i) == comma.at(0))
            {
                commaPosition = i;
                commas++;
            }
            if (entryString.at(i) == end.at(0))
            {
                endPosition = i;
                ends++;
                break;
            }
        }

        if (commas != 1 || ends != 1)
        {
            std::cout << "[ERROR]: Invalid entry in matrix. Correct format is ( , ) .\n";
            return std::complex<double>(0., 0.);
        }
        entryReal.assign(entryString, 1, commaPosition - 1);
        entryImag.assign(entryString, commaPosition + 1, endPosition - commaPosition - 1);

        try
        {
            return std::complex<double>(std::stod(entryReal), std::stod(entryImag));
        }
        catch (const std::exception &ia)
        {
            std::cerr << "[ERROR]: Invalid argument in matrix! Setting entry to zero!\n";
            return std::complex<double>(0., 0.);
        }
    }
    else
    {
        return std::complex<double>(0., 0.);
    }
    return std::complex<double>(0., 0.);
}