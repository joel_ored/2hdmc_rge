/**=============================================================================
 * @brief:: Functions and classes that uses Minuit
 * @author: Joel Oredsson
 *
 * Requires the Minuit2 library.
 * 
 *============================================================================*/
#pragma once

#include "THDM_complex.h"

#include "Minuit2/FCNGradientBase.h"
#include "Minuit2/MnUserParameters.h"


Base_generic fit_to_125mh(const THDM_complex &thdm, const int loopLvl);

Base_generic random_fitted_base_generic(THDM_complex &thdm, const int loopLvl,
                                        const gsl_rng *rng);

void improve_parameters(THDM_complex &thdm);

//------------------------------------------------------------------------------

/**
 * @brief: Specifies a physical observable that is being fitted
 *
 * Every observable has a value and a one sigma deviation (upper and lower).
 * Its contribution to the chi squared fit can be calculated with chiContrib.
 */
struct Observable {
  double _val, _sigmaLow, _sigmaHigh;

  Observable(const double val, const double sigmaLow, const double sigmaHigh)
      : _val(val), _sigmaLow(sigmaLow), _sigmaHigh(sigmaHigh) {}

  double chiContrib(const double valIn) const {
    if (valIn > _val)
      return (valIn - _val) * (valIn - _val) / (_sigmaHigh * _sigmaHigh);
    else
      return (valIn - _val) * (valIn - _val) / (_sigmaLow * _sigmaLow);
  }
};

//------------------------------------------------------------------------------

namespace ROOT {

namespace Minuit2 {

MnUserParameters create_minuit_user_parameters(const Base_generic &gen);

void retrieve_parameters_from_mup(Base_generic &gen,
                                  const MnUserParameters &mup);

//------------------------------------------------------------------------------

/**
 * @brief: Fcn function to be used to fit observables at EW scale
 */
class EwScaleFitter : public FCNBase {
public:
  EwScaleFitter();
  ~EwScaleFitter();

  /**
   * @brief: Operator used by Minuit
   *
   * @returns the chi squared fit calculated by chi2()
   */
  double operator()(const std::vector<double> &par) const;

  /**
   * @brief: Minuit function
   *
   * From Minuit manual:
   * Returns the value of up (default value = 1 : ), dening parameter errors.
   * MINUIT denes parameter errors as the change in parameter value required
   * to change the function value by up. Normally, for chisquared ts up = 1, 
   * and for negativ e log * likelihood, up = 0.
   * @returns 1.
   */

  double Up() const;

  
  void set_start_thdm(const THDM_complex &thdm);
  /**
   * @brief: Sets starting parameter point
   * @returns true if it is pert, unit and stable
   */
  bool set_base_generic(const Base_generic &gen);

  void set_loop_level(const int loopLvl);


private:
  /**
   * @brief: Initializes observables
   *
   * Fills _obs with experimental values.
   */
  void init();

  /**
   * @brief: Calculates the chi squared fit
   *
   * Compares lightest Higgs mass to experimental data in _obs.
   *
   * @returns total chi squared.
   */
  double chi2(const THDM_complex &thdm) const;
  

private:
  // @params:
  std::vector<Observable> _obs; // Experimental data
  THDM_complex _thdm;
  int _massLoopLvl;
};

//------------------------------------------------------------------------------

/**
 * @brief: Fcn function to be used to evolve to higher energies
 */
class HighScaleFitter : public FCNBase {
public:
  HighScaleFitter();
  ~HighScaleFitter();

  /**
   * @brief: Operator used by Minuit
   *
   * @returns the chi squared fit calculated by chi2()
   */
  double operator()(const std::vector<double> &par) const;

  /**
   * @brief: Minuit function
   *
   * From Minuit manual:
   * Returns the value of up (default value = 1 : ), dening parameter errors.
   * MINUIT denes parameter errors as the change in parameter value required
   * to change the function value by up. Normally, for chisquared ts up = 1, 
   * and for negativ e log * likelihood, up = 0.
   * @returns 1.
   */

  double Up() const;

  
  void set_start_thdm(const THDM_complex &thdm);

private:
  
  void init();

  void set_highScale(const double highScale);
  /**
   * @brief: Calculates the chi squared fit
   *
   * Compares lightest Higgs mass to experimental data in _obs.
   *
   * @returns total chi squared.
   */
  double chi2(const THDM_complex &thdm) const;
  

private:
  // @params:
  std::vector<Observable> _obs; // Experimental data
  THDM_complex _thdm;
};

} // namespace Minuit2
} // namespace ROOT
