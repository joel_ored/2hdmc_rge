/**=============================================================================
 * @brief: Random parameter scan of softly broken 2HDM
 * @author: Joel Oredsson
 * 
 * 
 *============================================================================*/
#include "ParameterScan.h"
#include "HelpFunctions.h"
#include "Structures.h"

#include <thread>
#include <iostream>

// RGE evolution options
static RgeConfig rgeConfig;
// Parent directory in output
static std::string scanTitle = "hybridScenarios_1e4";
static int maxPoints = 1e4;
static double minEnergy = 1e2;

void hybridScan(const int numberThreads, Range_hybrid &hybRange)
{
  ScanSystem scan(numberThreads);
  

  scan.set_directory(scanTitle + "/" + hybRange.name);
  scan.set_hybrid_basis_range(hybRange);
  scan.set_yukawa_type( hybRange.yukawaType);
  scan.set_minimum_energy(minEnergy);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::SOFTLY_BROKEN_Z2);
  scan.set_max_points(maxPoints);
  scan.set_spheno_config(false, 0);
  scan.create_model_files(false);
  scan.run_rge_scan();
}

std::vector<Range_hybrid> createHybridScenarios()
{
  std::vector<Range_hybrid> scenarios;

  // A1.1
  scenarios.emplace_back();
  scenarios.back().name = "A11";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {125.};
  scenarios.back().mH = {150.,600.};
  scenarios.back().cba = {0.1};
  scenarios.back().tanb = {1., 50.};
  scenarios.back().Z4 = {-2.};
  scenarios.back().Z5 = {-2.};
  scenarios.back().Z7 = {0.};

  // A2.1
  scenarios.emplace_back();
  scenarios.back().name = "A21";
  scenarios.back().yukawaType = TYPE_II;
  scenarios.back().mh = {125.};
  scenarios.back().mH = {150.,600.};
  scenarios.back().cba = {0.01};
  scenarios.back().tanb = {1., 50.};
  scenarios.back().Z4 = {-2.};
  scenarios.back().Z5 = {-2.};
  scenarios.back().Z7 = {0.};

  // B1.1
  scenarios.emplace_back();
  scenarios.back().name = "B11";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {65., 120.};
  scenarios.back().mH = {125.};
  scenarios.back().cba = {1.};
  scenarios.back().tanb = {1.5};
  scenarios.back().Z4 = {-5.};
  scenarios.back().Z5 = {-5.};
  scenarios.back().Z7 = {0.};

  // B1.2
  scenarios.emplace_back();
  scenarios.back().name = "B12";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {80., 120.};
  scenarios.back().mH = {125.};
  scenarios.back().cba = {0.9};
  scenarios.back().tanb = {1.5};
  scenarios.back().Z4 = {-5.};
  scenarios.back().Z5 = {-5.};
  scenarios.back().Z7 = {0.};

  // B2
  scenarios.emplace_back();
  scenarios.back().name = "B2";
  scenarios.back().yukawaType = TYPE_II;
  scenarios.back().mh = {65., 120.};
  scenarios.back().mH = {125.};
  scenarios.back().cba = {1.};
  scenarios.back().tanb = {1.5};
  scenarios.back().Z4 = {-5.};
  scenarios.back().Z5 = {-5.};
  scenarios.back().Z7 = {0.};

  // D11
  scenarios.emplace_back();
  scenarios.back().name = "D11";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 250., 500.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {-1.};
  scenarios.back().Z5 = {1.};
  scenarios.back().Z7 = {-1.};

  // D12
  scenarios.emplace_back();
  scenarios.back().name = "D12";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 250., 500.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {2.};
  scenarios.back().Z5 = {0.};
  scenarios.back().Z7 = {-1.};

  // D13
  scenarios.emplace_back();
  scenarios.back().name = "D13";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 250., 500.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {1.};
  scenarios.back().Z5 = {1.};
  scenarios.back().Z7 = {-1.};

  // D21
  scenarios.emplace_back();
  scenarios.back().name = "D21";
  scenarios.back().yukawaType = TYPE_II;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 250., 500.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {-1.};
  scenarios.back().Z5 = {1.};
  scenarios.back().Z7 = {-1.};

  // D22
  scenarios.emplace_back();
  scenarios.back().name = "D22";
  scenarios.back().yukawaType = TYPE_II;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 250., 500.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {2.};
  scenarios.back().Z5 = {0.};
  scenarios.back().Z7 = {-1.};

  // D23
  scenarios.emplace_back();
  scenarios.back().name = "D23";
  scenarios.back().yukawaType = TYPE_II;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 250., 500.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {1.};
  scenarios.back().Z5 = {1.};
  scenarios.back().Z7 = {-1.};

  // E11
  scenarios.emplace_back();
  scenarios.back().name = "E11";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 200., 300.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {-6.};
  scenarios.back().Z5 = {-2.};
  scenarios.back().Z7 = {0.};

  // E12
  scenarios.emplace_back();
  scenarios.back().name = "E12";
  scenarios.back().yukawaType = TYPE_I;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 200., 300.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {1.};
  scenarios.back().Z5 = {-3.};
  scenarios.back().Z7 = {0.};

  // E21
  scenarios.emplace_back();
  scenarios.back().name = "E21";
  scenarios.back().yukawaType = TYPE_II;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 200., 300.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {-6.};
  scenarios.back().Z5 = {-2.};
  scenarios.back().Z7 = {0.};

  // E22
  scenarios.emplace_back();
  scenarios.back().name = "E22";
  scenarios.back().yukawaType = TYPE_II;
  scenarios.back().mh = {125.};
  scenarios.back().mH = { 200., 300.};
  scenarios.back().cba = {0.};
  scenarios.back().tanb = { 2.};
  scenarios.back().Z4 = {1.};
  scenarios.back().Z5 = {-3.};
  scenarios.back().Z7 = {0.};

  return scenarios;
}
int main(int argc, char *argv[])
{
  int numberThreads = 1;

  rgeConfig.dataOutput = false; // false means no model files (in scan).
  rgeConfig.evolutionName = "none";
  rgeConfig.consoleOutput = false;
  rgeConfig.twoloop = true;        // Turns on two loop RGEs.
  rgeConfig.perturbativity = true; // Break RG evolution at perturbativity
  rgeConfig.stability = false;     // violation scale (false by default).
  rgeConfig.unitarity = false;
  rgeConfig.finalEnergyScale = 1.E18; // End of running scale.
  rgeConfig.steps = 150;

  if (argc == 2)
    numberThreads = atoi(argv[1]); // Change threads with console argument

  std::vector<Range_hybrid> scenarios = createHybridScenarios();
 
  for (unsigned int i=0; i < scenarios.size(); ++i){
    hybridScan(numberThreads, scenarios[i]);
  }

}
