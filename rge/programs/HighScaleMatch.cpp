/**=============================================================================
 * @brief: Evolving 2HDM up to GUT scale and then downwards to EW
 * @author: Joel Oredsson
 *
 *============================================================================*/
#include "SM_RGE.h"
#include "THDM_complex.h"

#include <iostream>
#include <complex>

int main(int argc, char *argv[]) {
  Timer timer;

  SM_RGE sm;
  sm.print_all();
  
  RgeConfig options;
  options.dataOutput = false;
  options.consoleOutput = true;
  options.evolutionName = "DemoRGE";
  options.twoloop = true;
  options.perturbativity = true;
  options.stability = false;
  options.unitarity = false;
  options.finalEnergyScale = 1e10;
  options.steps =  100;

  options.print();

  THDM_complex thdm(sm);
  Base_generic gen;
  gen.beta = 1.2452643742768286;
  gen.M12 =     std::complex<double>(21373.6,-0);       
  gen.Lambda1 =      0.242710 ;    
  gen.Lambda2 =      0.451976 ;    
  gen.Lambda3 =      -0.175837;    
  gen.Lambda4 =      0.173888 ;    
  gen.Lambda5 = std::complex<double>   (-0.161971,0);  
  gen.Lambda6 = std::complex<double> (0.,0.);
  gen.Lambda7 = std::complex<double> (0.,0.);

  thdm.set_param_gen(gen);
  thdm.set_yukawa_type(TYPE_I);

  thdm.set_rgeConfig(options);
  thdm.evolve();

  thdm.generate_random_soft_cp_conserved_potential(0, nullptr);

  thdm.print_potential();
  thdm.print_higgs_masses();

  options.finalEnergyScale = 173.34;
  thdm.set_rgeConfig(options);
  thdm.evolve();
  thdm.print_potential();
  thdm.print_higgs_masses();
  thdm.run_spheno(1);
  thdm.print_spheno_results();


  std::cout << "HighScaleMatch complete!\n\n";
}
