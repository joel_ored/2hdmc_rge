/**=============================================================================
 * @brief: Parameter scan of 2HDM at EW scale
 * @author: Joel Oredsson
 *============================================================================*/
#include "HelpFunctions.h"
#include "ParameterScan.h"

#include <iostream>

static const std::string scanTitle = "ewScan_1e1_typeI";
static const Z2symmetry yukawaType = TYPE_I;
static const int maxPoints = 1e5;

THDM_complex getParameterPoint()
{
  Range_hybrid hybRange;
  hybRange.name = "standard_range";
  hybRange.yukawaType = yukawaType;
  hybRange.mh = {120., 130.};
  hybRange.mH = {200., 1000.};
  hybRange.cba = {-0.5, 0.5};
  hybRange.tanb = {1.1, 50.};
  hybRange.Z4 = {- M_PI,  M_PI};
  hybRange.Z5 = {- M_PI,  M_PI};
  hybRange.Z7 = {- M_PI,  M_PI};

  SM_RGE sm;
  THDM_complex thdm(sm);
  thdm.set_logLevel(LOG_ERRORS);

  // Random number generator
  gsl_rng *rng = gsl_rng_alloc(gsl_rng_taus2);
  int seed = time(0) + 43 * 3715291;
  gsl_rng_set(rng, seed); // Seed the random number generator

  unsigned int tries = 0;
  for (;;)
  {
    ++tries;

    // print_prog_number("Try nr:", (double)tries );
    std::cout << "\nTry nr " << tries << std::endl;

    Base_hybrid hyb = hybRange.get_random_point(rng);

    if (!thdm.set_param_hybrid(hyb))
      continue;

    thdm.set_yukawa_type(yukawaType);

    if (!thdm.is_pert_unit_stab()){
      std::cout << "Not pert,unit and stab:(\n"; // DEBUG
      continue;
    }

    if (!thdm.run_spheno(2))
    {
      std::cout << "SPheno failed!\n";
      continue;
    }

    if (!thdm.is_within_spheno_limits()){
      std::cout << "Not within SPheno limits:(\n"; // DEBUG
      continue;
    }

    thdm.run_higgsBoundsSignals();

    if (thdm.is_allowed_by_HBHS())
      break;
  }

  std::cout << "\nFound a good parameter point on try number " << tries
            << "!\n";
  // Free memory allocation
  gsl_rng_free(rng);

  return thdm;
}

int main(int argc, char *argv[])
{
  Timer timer("Full program time:");

  THDM_complex thdm = getParameterPoint();

  thdm.print_all();
  // thdm.run_higgsBoundsSignals();
  // thdm.print_all();

  std::cout << "EwScan complete!\n";
}
