/**=============================================================================
 * @brief: Evolving parameter point from 1503.08216 as comparison
 * @author: Joel Oredsson
 *
 * In 1503.08216, they evolve the scenario H-4 from 1403.1264 at 2-loop order.
 * Here, we do the same.
 * 
 * H-4 parameters: 
 * tanb = 4.28
 * (b-a)/pi = 0.513
 * mH = 600
 * mA = 658
 * mHc = 591
 * M12 = 76900
 * 
 *============================================================================*/
#include "SM_RGE.h"
#include "THDM_complex.h"

#include <iostream>
#include <complex>
#include <string>

int main(int argc, char *argv[]) {
  Timer timer;
  
  RgeConfig rgeConfig;
  rgeConfig.dataOutput = true;
  rgeConfig.consoleOutput = true;
  rgeConfig.perturbativity = true;
  rgeConfig.stability = false;
  rgeConfig.unitarity = false;
  rgeConfig.finalEnergyScale = 1e18;
  rgeConfig.steps =  200;
  rgeConfig.evolutionName = "ReferencePoint";

  // Translate H-4 to Higgs basis
  // Base_higgs higgs;
  // higgs.beta = atan(4.28);
  // double cba = cos( 0.513 * M_PI);

  Base_generic gen;
  gen.beta = atan(4.28);
  gen.M12 =     std::complex<double> ( 76900.,0);
  gen.Lambda1 =      2.;            
  gen.Lambda2 =      0.5;            
  gen.Lambda3 =      -1.;            
  gen.Lambda4 =      1.2;            
  gen.Lambda5 = std::complex<double>  (-1.2,0.); 
  gen.Lambda6 = std::complex<double> (0.,0.);
  gen.Lambda7 = std::complex<double> (0.,0.);

  SM_RGE sm;
  THDM_complex thdm(sm);

  thdm.set_rgeConfig(rgeConfig);
  thdm.set_param_gen(gen);

  thdm.set_yukawa_type(TYPE_II);

  thdm.run_spheno(1);
  thdm.print_all();

  thdm.save_current_state();

  // 1.) 1-loop evolution
  // rgeConfig.evolutionName = "1dPoint";
  rgeConfig.twoloop = false;
  thdm.set_rgeConfig(rgeConfig);

  thdm.evolve();

  // 2.) 2-loop evolution
  thdm.reset_to_saved_state();

  // rgeConfig.evolutionName = "1dPoint/twoLoop";
  rgeConfig.twoloop = true;
  thdm.set_rgeConfig(rgeConfig);

  thdm.evolve();

  std::cout << "Example scenario from 1503.08216 complete!\n\n";
}
