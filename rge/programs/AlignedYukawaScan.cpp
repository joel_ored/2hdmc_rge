/**=============================================================================
 * @brief: Random parameter scan of aligned Yukawa 2HDM parameter point
 * @author: Joel Oredsson, aug 2018
 *
 * The point of this scan is to see the effects of deviating from a symmetric
 * Z2 Yukawa sector at the EW scale, in the RG evolution.
 *
 * The starting point is a certain parameter point that is softly Z2 broken and
 * valid all the way up to the Planck scale. The point was derived with a
 * particular Z2 Yukawa type.
 *
 * The rho matrices are set at the EW scale as aligned matrices, i.e.
 * proportional to the kappa matrices with some real coefficient aF.
 *
 * Each aF is varied in an independent scan, which makes it a 1D scan
 * essantially. The parameter point is then evolved and the results are stored
 * as usual. Of special interest is the induced Z2 breaking parameters Lambda6,
 * Lambda7 and non-diagonal rho elements.
 *
 * Since the coupling of the scalar and Yukawa sector is determined by the
 * loop order of the RGEs, we scan using both 1-loop and 2-loop scan seperately.
 *
 *============================================================================*/
#include "HelpFunctions.h"
#include "ParameterScan.h"
#include "Structures.h"

#include <iostream>
#include <thread>

// RGE evolution options
static RgeConfig rgeConfig;

// Parent directory in output
static std::string scanTitle;

static const double minEnergy = 1e10;
static const int maxPoints = 1e4; // Number of parameter points

// Scenarios of softly broken Z2 2HDMs that are good all the way up to the
// Planck scale. These were tested with Yukawa symmetry contained in z2SymVec.
static std::vector<Base_generic> genVec;
static std::vector<Z2symmetry> z2SymVec;

// Prints the Z2 symmetric values of aF as function of tanb
void print_aF(const double tanb)
{
  printf("Aligned params, aF, for tanb = %16.8e \n", tanb);
  printf("Type-I: \n");
  printf("aU =  %16.8e, aD = %16.8e, aL = %16.8e \n", 1 / tanb, 1 / tanb,
         1 / tanb);
  printf("Type-II: \n");
  printf("aU =  %16.8e, aD = %16.8e, aL = %16.8e \n", 1 / tanb, -tanb, -tanb);
  printf("Type-III: \n");
  printf("aU =  %16.8e, aD = %16.8e, aL = %16.8e \n", 1 / tanb, -tanb,
         1 / tanb);
  printf("Type-IV: \n");
  printf("aU =  %16.8e, aD = %16.8e, aL = %16.8e \n", 1 / tanb, 1 / tanb,
         -tanb);
}
void initBaseScenarios()
{
  genVec.resize(2);

  genVec[0].beta = 1.15577; // tanb =  2.26953;
  genVec[0].M12 = std::complex<double>(35381.1, 0.);
  genVec[0].Lambda1 = 0.288282;
  genVec[0].Lambda2 = 0.433148;
  genVec[0].Lambda3 = 0.00787984;
  genVec[0].Lambda4 = -0.123361;
  genVec[0].Lambda5 = -0.0901608;
  genVec[0].Lambda6 = std::complex<double>(0., 0.);
  genVec[0].Lambda7 = std::complex<double>(0., 0.);
  z2SymVec.push_back(TYPE_I);

  genVec[1].beta = 1.46713; // tanb =  9.61166
  genVec[1].M12 = std::complex<double>(3132.85, 0.);
  genVec[1].Lambda1 = 0.413702;
  genVec[1].Lambda2 = 0.263926;
  genVec[1].Lambda3 = 0.13313;
  genVec[1].Lambda4 = -0.0444794;
  genVec[1].Lambda5 = 0.29586;
  genVec[1].Lambda6 = std::complex<double>(0., 0.);
  genVec[1].Lambda7 = std::complex<double>(0., 0.);
  z2SymVec.push_back(TYPE_I);
}

void aUScan(const int numberThreads, const int maxPoints, const int genID)
{
  ScanSystem scan(numberThreads);

  if (rgeConfig.twoloop)
    scan.set_directory(scanTitle + "/ALIGNED_YUKAWA_aU_2loop");
  else
    scan.set_directory(scanTitle + "/ALIGNED_YUKAWA_aU_1loop");

  scan.set_base_generic(genVec[genID]);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::ALIGNED_YUKAWA);
  scan.set_aF_scan(UP, z2SymVec[0]);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(false, 0);
  scan.create_model_files(false);

  scan.run_rge_scan();
}

void aDScan(const int numberThreads, const int maxPoints, const int genID)
{
  ScanSystem scan(numberThreads);

  if (rgeConfig.twoloop)
    scan.set_directory(scanTitle + "/ALIGNED_YUKAWA_aD_2loop");
  else
    scan.set_directory(scanTitle + "/ALIGNED_YUKAWA_aD_1loop");

  scan.set_base_generic(genVec[genID]);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::ALIGNED_YUKAWA);
  scan.set_aF_scan(DOWN, z2SymVec[0]);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(false, 0);
  scan.create_model_files(false);

  scan.run_rge_scan();
}

void aLScan(const int numberThreads, const int maxPoints, const int genID)
{
  ScanSystem scan(numberThreads);

  if (rgeConfig.twoloop)
    scan.set_directory(scanTitle + "/ALIGNED_YUKAWA_aL_2loop");
  else
    scan.set_directory(scanTitle + "/ALIGNED_YUKAWA_aL_1loop");

  scan.set_base_generic(genVec[genID]);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::ALIGNED_YUKAWA);
  scan.set_aF_scan(LEPTON, z2SymVec[0]);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(false, 0);
  scan.create_model_files(false);

  scan.run_rge_scan();
}

void softZ2_aligned(const int numberThreads, const int maxPoints)
{
  ScanSystem scan(numberThreads);

  Range_hybrid hybRange;
  hybRange.name = "standard_range";
  hybRange.yukawaType = NO_SYMMETRY;
  hybRange.mh = {122.,128.};
  hybRange.mH = {150.,1000.};
  hybRange.cba = {-0.3,0.3};
  hybRange.tanb = {1.1,50.};
  hybRange.Z4 = {-1.5, 1.5};
  hybRange.Z5 = {-0.8, 0.8};
  hybRange.Z7 = {-0.5, 0.5};

  // SOFTLY_BROKEN_Z2_ALIGNED uses a fixed tanb:
  Base_generic gen;
  gen.beta = atan(3.);
  scan.set_base_generic(gen);

  scan.set_directory(scanTitle + "/SOFTLY_BROKEN_Z2_ALIGNED" + "_tanbIs10");
  scan.set_hybrid_basis_range(hybRange);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::SOFTLY_BROKEN_Z2_ALIGNED);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(false, 0);
  scan.create_model_files(false);
  scan.run_rge_scan();
}

int main(int argc, char *argv[])
{
  Timer timer;
  int numberThreads = 1;

  rgeConfig.dataOutput = false; // false means no model files (in scan).
  rgeConfig.evolutionName = "none";
  rgeConfig.consoleOutput = false;
  rgeConfig.twoloop = true;       // Turns on two loop RGEs.
  rgeConfig.perturbativity = true; // Break RG evolution at perturbativity
  rgeConfig.stability = true;     // violation scale (false by default).
  rgeConfig.unitarity = true;
  rgeConfig.finalEnergyScale = 1.E18; // End of running scale.
  rgeConfig.steps = 250;

  if (argc == 2)
    numberThreads = atoi(argv[1]); // Change threads with console argument

  initBaseScenarios();
  

  // 1-dimensional scans of aF with specific parameter points:
  // Looping through the parameter points in genVec
  // for (unsigned int i = 1; i < genVec.size(); ++i)
  // {
  //   scanTitle = "yukawaScan_gen" + std::to_string(i + 1) + "_" + stringAuto(maxPoints);

  //   std::cout << "Scenario " << std::to_string(i + 1) << ":\n"
  //             << genVec[i] << std::endl;
  //   print_aF(tan(genVec[i].beta));

  //   // Each scan varies one aF
  //   rgeConfig.twoloop = false; // Turns off two loop RGEs.
  //   aUScan(numberThreads, maxPoints, i);
  //   aDScan(numberThreads, maxPoints, i);
  //   aLScan(numberThreads, maxPoints, i);

  //   // Using 2-loop RGEs
  //   rgeConfig.twoloop = true; // Turns on two loop RGEs.
  //   aUScan(numberThreads, maxPoints, i);
  //   aDScan(numberThreads, maxPoints, i);
  //   aLScan(numberThreads, maxPoints, i);
  // }

  // // 2-dimensional aligned scan with random softly broken Z2 parameter point
  // // Although with fixed tanb.
  scanTitle = "yukawaScanSoft_tanb3_" + stringAuto(maxPoints);
  softZ2_aligned(numberThreads, maxPoints);
}
