/**=============================================================================
 * @brief: Random parameter scan of 2HDM
 * @author: Joel Oredsson, aug 2018
 * 
 * Random parameter scan of the 2HDM with different levels of Z_2 symmetry 
 * breaking:
 *  i.) Exact Z_2 symmetry (automatically CP conserving)
 *  ii.) Softly broken Z_2 symmetry (CP conservation assumed, i.e. real 
 *       parameters).
 *  iii.) Hard broken Z_2 symmetry with also assumed CP conservation.
 * 
 * Algorithm:
 *   1.) Generate random parameter point at top mass scale.
 *   2.) Accept the parameter point if it satisfies:
 *         -> Tree-lvl Pert, unit, stab
 *         -> 2-loop m_h mass = 125 GeV
 *   3.) Perform RG evolution.
 *   4.) Save EW parameter points and results of RG evolution.
 * 
 * The only difference in each case is the parameter point generation.
 * 
 * Can set number of threads used in scan with console argument (default set to
 * one).
 * 
 *============================================================================*/
#include "ParameterScan.h"
#include "HelpFunctions.h"
#include "Structures.h"

#include <thread> 
#include <iostream>

// RGE evolution options
static RgeConfig rgeConfig;
// Parent directory in output
static const std::string scanTitle = "randomScan_1e5_typeII";
static const Z2symmetry yukawaType = TYPE_II;
static const double minEnergy = 200.;
static const int maxPoints = 1e5;

void exactZ2(const int numberThreads, const int maxPoints)
{
  ScanSystem scan(numberThreads);

  Range_hybrid hybRange;
  hybRange.name = "standard_range";
  hybRange.yukawaType = yukawaType;
  hybRange.mh = {10.,130.};
  hybRange.mH = {150.,600.};
  hybRange.cba = {-0.5,0.5};
  hybRange.tanb = {1.1,50.};
  hybRange.Z4 = {-M_PI, M_PI};
  hybRange.Z5 = {-M_PI, M_PI};
  hybRange.Z7 = {-M_PI, M_PI};

  scan.set_directory(scanTitle + "/EXACT_Z2");
  scan.set_hybrid_basis_range(hybRange);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::EXACT_Z2);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(true, 1);
  scan.create_model_files(false);
  scan.run_rge_scan();
}

void softZ2(const int numberThreads, const int maxPoints)
{
  ScanSystem scan(numberThreads);

  Range_hybrid hybRange;
  hybRange.name = "standard_range";
  hybRange.yukawaType = yukawaType;
  hybRange.mh = {10.,130.};
  hybRange.mH = {150.,1000.};
  hybRange.cba = {-0.5,0.5};
  hybRange.tanb = {1.1,50.};
  hybRange.Z4 = {-M_PI, M_PI};
  hybRange.Z5 = {-M_PI, M_PI};
  hybRange.Z7 = {-M_PI, M_PI};

  scan.set_directory(scanTitle + "/SOFTLY_BROKEN_Z2");
  scan.set_hybrid_basis_range(hybRange);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::SOFTLY_BROKEN_Z2);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(true, 1);
  scan.create_model_files(false);
  scan.run_rge_scan();
}

void hardZ2(const int numberThreads, const int maxPoints)
{
  ScanSystem scan(numberThreads);

  Range_hybrid hybRange;
  hybRange.name = "standard_range";
  hybRange.yukawaType = yukawaType;
  hybRange.mh = {10.,130.};
  hybRange.mH = {150.,1000.};
  hybRange.cba = {-0.5,0.5};
  hybRange.tanb = {1.1,50.};
  hybRange.Z4 = {-M_PI, M_PI};
  hybRange.Z5 = {-M_PI, M_PI};
  hybRange.Z7 = {-M_PI, M_PI};

  scan.set_directory(scanTitle + "/HARD_BROKEN_Z2");
  scan.set_hybrid_basis_range(hybRange);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::HARD_BROKEN_Z2);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(true, 1);
  scan.create_model_files(false);

  scan.run_rge_scan();
  
}

void flat(const int numberThreads, const int maxPoints)
{
  ScanSystem scan(numberThreads);

  Range_hybrid hybRange;
  hybRange.name = "standard_range";
  hybRange.yukawaType = yukawaType;
  hybRange.mh = {10.,130.};
  hybRange.mH = {150.,1000.};
  hybRange.cba = {-0.5,0.5};
  hybRange.tanb = {1.1,50.};
  hybRange.Z4 = {-M_PI, M_PI};
  hybRange.Z5 = {-M_PI, M_PI};
  hybRange.Z7 = {-M_PI, M_PI};

  scan.set_directory(scanTitle + "/FLAT_Z2_CP_CONSERVED");
  scan.set_hybrid_basis_range(hybRange);
  scan.set_rgeConfig(rgeConfig);
  scan.set_scanType(ScanType::FLAT_Z2_CP_CONSERVED);
  scan.set_max_points(maxPoints);
  scan.set_minimum_energy(minEnergy);
  scan.set_spheno_config(false, 0);
  scan.create_model_files(false);

  scan.run_rge_scan();
  
}


int main(int argc, char *argv[])
{
  Timer timer;
  int numberThreads = 1;

  rgeConfig.dataOutput = false; // false means no model files (in scan).
  rgeConfig.evolutionName = "none";
  rgeConfig.consoleOutput = false;
  rgeConfig.twoloop = true;        // Turns on two loop RGEs.
  rgeConfig.perturbativity = true; // Break RG evolution at perturbativity
  rgeConfig.stability = false;     // violation scale (false by default).
  rgeConfig.unitarity = false;
  rgeConfig.finalEnergyScale = 1.E18; // End of running scale.
  rgeConfig.steps = 200;

  if (argc == 2)
    numberThreads = atoi(argv[1]); // Change threads with console argument

  // exactZ2(numberThreads, maxPoints);
  // softZ2(numberThreads, maxPoints);
  // hardZ2(numberThreads, maxPoints);
  flat( numberThreads, maxPoints);
  
}
