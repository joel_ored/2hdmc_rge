#include "FileSystem.h"
#include "HelpFunctions.h"
#include "RgeModel.h"
#include "THDM_complex.h"

#include <Eigen/Dense>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>

FileSystem::FileSystem() : _created(0) {}

FileSystem::FileSystem(const std::string &dir_in)
    : _created(0), _directory(dir_in + "/") {}

FileSystem::~FileSystem()
{
  for (int i = 0; i < _fileStreams.size(); i++)
    delete _fileStreams[i];

  _fileStreams.clear();
}

void FileSystem::set_directory(const std::string &dir_in)
{
  _directory = dir_in + "/";
}

std::string FileSystem::get_directory() const
{
  return _directory;
}
bool FileSystem::create_multiple_files(const std::vector<std::string> &vec_in)
{
  for (int i = 0; i < vec_in.size(); i++)
  {
    if (create_file(vec_in[i]) == false)
      return false;
  }
  return true;
}

bool FileSystem::create_file(const std::string &dir_in, bool append)
{
  // Create an ofstream
  _fileStreams.push_back(new std::ofstream);

  // Maps the directory of the file to an index of the _fileStreams
  _fileMap[dir_in] = _created;

  // If the file does not exist -> create it
  if (file_exists(_directory + dir_in) == false)
  {
    create_folder_path(extract_folder_path(_directory + dir_in));
  }

  // Open file in append or overwrite mode
  if (append)
    _fileStreams[_created]->open(_directory + dir_in, std::fstream::app);
  else
    _fileStreams[_created]->open(_directory + dir_in, std::ios::out);

  _created++;
  return true;
}
bool FileSystem::open_file(const std::string &dir_in, bool append)
{
  /* Maps the directory of the file to an index of the _fileStreams
     (the index is search->second). */
  auto search = _fileMap.find(dir_in);
  // Open file
  if (search != _fileMap.end())
  {
    if (append)
      _fileStreams[search->second]->open(_directory + dir_in,
                                         std::fstream::app);
    else
      _fileStreams[search->second]->open(_directory + dir_in, std::ios::out);
    return true;
  }
  std::cout << "\n[ERROR]: could not open file " << dir_in.c_str() << std::endl;
  return false;
}

bool FileSystem::create_single_file(const std::string &fileName,
                                    const std::string &text) const
{
  std::ofstream streamTemp; // Temporary ofstream
  // Tries to open the ofstream
  streamTemp.open(_directory + fileName, std::ios::out);
  streamTemp << "";

  // If the file does not exist -> create it
  if (file_exists(_directory + fileName) == false)
  {
    create_folder_path(extract_folder_path(_directory + fileName));
    streamTemp.open(_directory + fileName);
  }
  streamTemp << text; // Writes the text_in to file
  streamTemp.close();
}

bool FileSystem::add_line(const std::string &name_in, const double &t_in,
                          const Eigen::Matrix3cd &matrix_in)
{
  auto search = _fileMap.find(name_in);
  if (search != _fileMap.end())
  {
    *_fileStreams[search->second] << exp(t_in);
    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
      {
        *_fileStreams[search->second] << "\t" << abs(matrix_in(i, j));
      }
    }
    *_fileStreams[search->second] << "\n";
    return true;
  }
  std::cout << "\n[ERROR]: could not add new line to " << name_in.c_str()
            << std::endl;
  return false;
}

void FileSystem::close_streams()
{
  for (int i = 0; i < _created; i++)
  {
    _fileStreams[i]->close();
  }
}
void FileSystem::flush_streams()
{
  for (int i = 0; i < _created; i++)
  {
    _fileStreams[i]->flush();
  }
}

void FileSystem::create_model_file(const RgeModel &model,
                                   const std::string &fileName) const
{
  Table::change_colorMode(false);

  std::streambuf *psbuf, *backup;
  std::ostringstream stringStream(std::ostringstream::ate);

  backup = std::cout.rdbuf(); // back up cout's streambuf

  psbuf = stringStream.rdbuf(); // get file's streambuf
  std::cout.rdbuf(psbuf);       // assign streambuf to cout

  std::cout << "############################################################\n"
            << "# " << model.get_name() << " 2HDMC model file\n"
            << "# Created: " << date() << "\n"
            << "############################################################\n";

  model.get_rgeConfig().print();

  std::cout << "\n#Parameters of the model:\n";

  model.print_all();

  std::cout << "# Parameters y[] as a double array:\n";
  std::cout << "RenormScale: " << model.get_renormalization_scale()
            << std::endl;
  double y[model.get_numParams()];
  model.set_y(y);

  for (int i = 0; i < model.get_numParams(); i++)
  {
    std::cout << "@ y[" << std::to_string(i) << "]= " << stringAuto(y[i])
              << "\n";
  }

  create_single_file(fileName + ".txt", stringStream.str());

  std::cout.rdbuf(backup); // restore cout's original streambuf
  Table::change_colorMode(true);
}
void FileSystem::create_model_file(const RgeModel &model,
                                   const RgeResults &rgeResults,
                                   const std::string &fileName) const
{
  Table::change_colorMode(false);

  std::streambuf *psbuf, *backup;
  std::ostringstream stringStream(std::ostringstream::ate);

  backup = std::cout.rdbuf(); // back up cout's streambuf

  psbuf = stringStream.rdbuf(); // get file's streambuf
  std::cout.rdbuf(psbuf);       // assign streambuf to cout

  std::cout << "############################################################\n"
            << "# " << model.get_name() << " model file\n"
            << "# Created: " << date() << "\n"
            << "############################################################\n";

  rgeResults.print(false);
  model.get_rgeConfig().print();

  std::cout << "\n#Parameters of the model:\n";

  model.print_all();

  std::cout << "# Parameters y[] as a double array:\n";
  std::cout << "RenormScale: " << model.get_renormalization_scale()
            << std::endl;
  double y[model.get_numParams()];
  model.set_y(y);

  for (int i = 0; i < model.get_numParams(); i++)
  {
    std::cout << "@ y[" << std::to_string(i) << "]= " << stringAuto(y[i])
              << "\n";
  }

  create_single_file(fileName + ".txt", stringStream.str());

  std::cout.rdbuf(backup); // restore cout's original streambuf
  Table::change_colorMode(true);
}

bool FileSystem::load_model_file(RgeModel &model, const char *modelFile) const
{
  if (modelFile == nullptr)
  {
    std::cout << "[ERROR]: nullptr sent to load_model_file.\n";
    return false;
  }

  std::cout << "Loading " << modelFile << std::endl;

  std::ifstream modelFileStream(modelFile, std::ios_base::in);

  if (!modelFileStream)
  {
    std::cout << "[ERROR]: Couldn't open " << modelFile << std::endl;
    return false;
  }

  std::stringstream sStream;
  sStream << modelFileStream.rdbuf();

  std::vector<double> yVec;
  std::string y = "@";

  std::string renormScaleString = "RenormScale:";
  double renormScale = 0.;

  while (sStream)
  {
    std::string temp;
    sStream >> temp;

    if (temp[0] == y[0])
    {
      sStream >> temp; // Removes the "y[i]=" chars.
      double tD;
      sStream >> tD;
      yVec.push_back(tD);
    }
    else if (temp == renormScaleString)
    {
      sStream >> renormScale;
    }
  }

  // Copy yVec to an array..
  double yArr[yVec.size()];
  for (int i = 0; i < yVec.size(); i++)
  {
    yArr[i] = yVec[i];
    // std::cout << "y[" << i << "] = " << yArr[i] << std::endl;
  }
  model.reset(yArr, renormScale);
  // for (int i=0; i < yVec.size(); i++){
  // 	std::cout << "y[" << i << "] = " << yVec[i] << std::endl;
  // }
  return true;
}
