/**=============================================================================
 * @brief: Demo of RG evolution of a 2HDM
 * @author: Joel Oredsson
 *
 * Evolves a CP conserving type II 2HDM in energy up to the Planck scale.
 * ! NO, the parameter point is no good :(
 * 
 * http://2hdmc.hepforge.org
 *============================================================================*/
#include "SM_RGE.h"
#include "THDM_complex.h"

// #include "THDM_fitter.h"

#include <iostream>
#include <complex>
 
int main(int argc, char *argv[]) {
  Timer timer; 
  
  Base_generic gen;
  

  // gen.beta = 1.2452643742768286;
  // gen.M12 =     std::complex<double>(21373.6,-0);       
  // gen.Lambda1 =      0.242710 ;    
  // gen.Lambda2 =      0.451976 ;    
  // gen.Lambda3 =      -0.175837;    
  // gen.Lambda4 =      0.173888 ;    
  // gen.Lambda5 = std::complex<double>   (-0.161971,0);    
  gen.beta = 1.46713; // tanb =  9.61166
  gen.M12 = std::complex<double>(3132.85, 0.);
  gen.Lambda1 = 0.413702;
  gen.Lambda2 = 0.263926;
  gen.Lambda3 = 0.13313;
  gen.Lambda4 = -0.0444794;
  gen.Lambda5 = std::complex<double> (0.29586, 0.01);
  gen.Lambda6 = std::complex<double>(0., 0.);
  gen.Lambda7 = std::complex<double>(0., 0.);

  // gen.beta =  0.863872;
  // // gen.tanb =  1.17069
  // gen.M12 =  std::complex<double> (11435.4, 0.);
  // gen.Lambda1 =  0.468466;
  // gen.Lambda2 =  0.754759;
  // gen.Lambda3 =  -0.150675;
  // gen.Lambda4 =  0.0035644;
  // gen.Lambda5 =  std::complex<double>(0.0692585   ,0.);
  // gen.Lambda6 =  std::complex<double>(-0.0831947,0.);
  // gen.Lambda7 =  std::complex<double>(0.206476,0.);

  SM_RGE sm;
  sm.print_all();
  
  RgeConfig options; 
  options.dataOutput = true;
  options.consoleOutput = true;
  options.evolutionName = "DemoRGE_gen2";
  options.twoloop = true;
  options.perturbativity = true;
  options.stability = false;
  options.unitarity = false;
  options.finalEnergyScale = 1e18;
  options.steps =  100;

  options.print();

  
  THDM_complex thdm(sm);
  thdm.set_param_gen(gen);
  thdm.set_rgeConfig(options);

  // Imposes the type I Z_2 symmetry.
  thdm.set_yukawa_type(TYPE_I);

  thdm.run_spheno(1);

  // #if defined HiggsBounds
  // thdm.run_higgsBoundsSignals();
  // #endif
  // thdm.calc_rgeResults();
  
  thdm.print_all();

  // thdm.save_current_state();
  thdm.evolve();

  std::cout << "Parameters after evolution: \n";
  thdm.print_all();

  // thdm.get_largest_diagonal_rF();
  // thdm.get_largest_nonDiagonal_rF();
  // thdm.get_largest_lambda();

  // thdm.reset_to_saved_state();
  // thdm.set_yukawa_type(TYPE_II);

  // thdm.run_spheno(1);

  // thdm.print_all();

  // thdm.evolve();

  // thdm.print_all();

  // thdm.get_largest_diagonal_rF();
  // thdm.get_largest_nonDiagonal_rF();
  // thdm.get_largest_lambda();

  std::cout << "DemoRGE complete!\n\n";
}
