/**=============================================================================
 * @brief:: Misc structures
 * @author: Joel Oredsson
 * 
 *============================================================================*/
#pragma once

#include "HelpFunctions.h"
// #include "THDM.h"
// #include "SM.h"

#include <fstream>
#include <iostream>
#include <complex>
#include <string>
#include <Eigen/Dense>
#include <gsl/gsl_rng.h>
#include <mutex>

/**
* @brief Options for RG evolutions
* 
* The RGE evolution is configured with a RGE_config struct.
* The Results of the RGE evolution is saved in a RgeResults struct.
*/
struct RgeConfig
{
  bool twoloop; //set to true to use two-loop RGEs

  bool perturbativity; // if these = true, enforces
  bool stability;      // that the RGE evolution breaks when violating them.
  bool unitarity;

  bool consoleOutput;        //If false, hides output to console
  bool dataOutput;           //prints evolution data to files
  std::string evolutionName; //data is printed to "output/evolutionName"
  double finalEnergyScale;   // Final energy scale for RG evolution.
  int steps;                 // steps for RGE evolution (distributed
                             // logarithmically)

  RgeConfig() : twoloop(false),
                perturbativity(true),
                stability(true),
                unitarity(true),
                consoleOutput(true),
                dataOutput(false),
                evolutionName("evolution"),
                finalEnergyScale(1e18),
                steps(100)
  {
  }

  void print() const
  {
    std::cout << "RGE options:\n";

    std::cout << "Evolution name: " << evolutionName << std::endl;

    auto check = [](const std::string &message, const bool boolean) {
      std::cout << message;
      if (boolean)
        std::cout << "true\n";
      else
        std::cout << "false\n";
    };
    check("Data output: ", dataOutput);
    check("Console output: ", consoleOutput);
    check("2-loop: ", twoloop);
    check("Break at perturbativity violation: ", perturbativity);
    check("Break at unitarity violation: ", unitarity);
    check("Break at stability violation: ", stability);

    std::cout << "End energy: " << finalEnergyScale << std::endl;
    std::cout << "ODE steps: " << steps << std::endl;
  }
};

//------------------------------------------------------------------------------

/**
 * @brief: Results of RG evolution
 * 
 * Struct that contains all information of a models
 * RG evolution.
 **/
struct RgeResults
{
  // Start and finish energy for RGE evolution
  double e0, ef;

  // The breakdown energy for perturbativity etc.
  // They are initialized to -1 to mark that they have not been violated.
  double ef_pert;
  double ef_unit;
  double ef_stab;

  bool evolved; // true if the THDM_complex has been performed RGE_evolve

  // These are true if the model has
  // broken them during RGE evolion
  bool LandauViolation;
  bool perturbativityViolation;
  bool unitarityViolation;
  bool stabilityViolation;

  // Constructor
  RgeResults() : e0(0),
                 ef(0),
                 ef_pert(-1.0),
                 ef_unit(-1.0),
                 ef_stab(-1.0),
                 evolved(false),
                 LandauViolation(false),
                 perturbativityViolation(false),
                 unitarityViolation(false),
                 stabilityViolation(false)
  {
  }

  // Copy constructor
  RgeResults(const RgeResults &rge)
  {
    e0 = rge.e0;
    ef = rge.ef;
    ef_pert = rge.ef_pert;
    ef_unit = rge.ef_unit;
    ef_stab = rge.ef_stab;
    evolved = rge.evolved;
    LandauViolation = rge.LandauViolation;
    perturbativityViolation = rge.perturbativityViolation;
    unitarityViolation = rge.unitarityViolation;
    stabilityViolation = rge.stabilityViolation;
  }

  void reset()
  {
    e0 = 0.;
    ef = 0.;
    ef_pert = -1.0;
    ef_unit = -1.0;
    ef_stab = -1.0;
    evolved = false;
    LandauViolation = false;
    perturbativityViolation = false;
    unitarityViolation = false;
    stabilityViolation = false;
  }

  // Print to console
  void print(const bool fancyStyle = true) const
  {
    if (!evolved)
    {
      std::cout << "RGE: \x1B[33munevolved.\x1B[0m\n";
      return;
    }
    if (fancyStyle)
    {
      // \x1B[31m = bold red text color
      // \x0B[32m = green text color
      // \x1B[0m restores it.
      std::cout << "\n\x1B[4mRGE Results\x1B[0m" << std::endl;
      std::cout << "Evolved from " << e0 << " GeV to " << ef << " GeV: "
                << std::endl;
      if (LandauViolation)
        std::cout << "\x1B[1;31mLandau pole: " << ef << " GeV\x1B[0m"
                  << std::endl;
      else
        std::cout << "\x1B[0;32mNo Landau pole!\x1B[0m" << std::endl;
      if (perturbativityViolation)
        std::cout << "\x1B[1;31mPerturbativity breakdown: " << ef_pert
                  << " GeV\x1B[0m" << std::endl;
      else
        std::cout << "\x1B[0;32mPerturbativity satisfied!\x1B[0m" << std::endl;
      if (unitarityViolation)
        std::cout << "\x1B[1;31mTree-lvl unitarity breakdown: " << ef_unit
                  << " GeV\x1B[0m" << std::endl;
      else
        std::cout << "\x1B[0;32mUnitarity satisfied!\x1B[0m" << std::endl;
      if (stabilityViolation)
        std::cout << "\x1B[1;31mStability breakdown: " << ef_stab
                  << " GeV\x1B[0m" << std::endl;
      else
        std::cout << "\x1B[0;32mStability satisfied!\x1B[0m" << std::endl;
        
      std::cout << "\n";
    }
    else
    {
      std::cout << "\nResults" << std::endl;
      std::cout << "Evolved from " << e0 << " GeV to " << ef << " GeV: "
                << std::endl;
      if (LandauViolation)
        std::cout << "Landau pole: " << ef << " GeV" << std::endl;
      else
        std::cout << "No Landau pole!" << std::endl;
      if (perturbativityViolation)
        std::cout << "Perturbativity breakdown: " << ef_pert << " GeV"
                  << std::endl;
      else
        std::cout << "Perturbativity satisfied!" << std::endl;
      if (unitarityViolation)
        std::cout << "Tree-lvl unitarity breakdown: " << ef_unit << " GeV"
                  << std::endl;
      else
        std::cout << "Unitarity satisfied!" << std::endl;
      if (stabilityViolation)
        std::cout << "Stability breakdown: " << ef_stab << " GeV" << std::endl;
      else
        std::cout << "Stability satisfied!" << std::endl;
      std::cout << "\n";
    }
  }
};


//------------------------------------------------------------------------------

/**
 * @Brief Gradient information
 * 
 * The gradient of the breakdown energy of a THDM_complex as a function of the 
 * generic parameters can be saved in Gradient, which stores information about 
 * the calculation.
 * */
struct Gradient
{
  int parameters;
  double ef_start;
  double DELTA; // Sets the stepsize when calculating derivatives
  std::vector<double> derivative;
  std::vector<double> energy;

  // Constructor
  Gradient() : parameters(12), ef_start(0.), DELTA(0.)
  {
    derivative.resize(parameters);
    energy.resize(parameters);
  }

  // Function that returns max derivative
  int maxDerivative() const
  {
    double max = 0.;
    int maxIndex = 0;

    for (int i = 0; i < parameters; i++)
    {
      if (std::abs(derivative[i]) > max)
      {
        maxIndex = i;
        max = std::abs(derivative[i]);
      }
    }
    return maxIndex;
  }

  // Normalizes the gradient vector
  void normalize()
  {
    double sum = 0.;

    for (int i = 0; i < parameters; i++)
    {
      sum += derivative[i] * derivative[i];
    }
    if (sum != 0.)
    {
      double sumSquaredInv = 1.0 / sqrt(sum);
      for (int i = 0; i < parameters; i++)
      {
        derivative[i] *= sumSquaredInv;
      }
    }
    else
      std::cout << "[WARNING]: couldn't normalize gradient. It has length 0!\n";
  }

  // Print function
  void print() const
  {
    std::cout << "Gradient at point with breakdown energy " << ef_start << std::endl;
    Table tab(3);
    tab.add_row(std::vector<std::string>{
                    "Parameter", "def/dParameter", "Delta ef"},
                true);
    tab.add_row(std::vector<std::string>{
        "M222", stringAuto(derivative[0]), stringAuto(energy[0] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Lambda1", stringAuto(derivative[1]), stringAuto(energy[1] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Lambda2", stringAuto(derivative[2]), stringAuto(energy[2] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Lambda3", stringAuto(derivative[3]), stringAuto(energy[3] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Lambda4", stringAuto(derivative[4]), stringAuto(energy[4] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Re(Lambda5)", stringAuto(derivative[5]),
        stringAuto(energy[5] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Im(Lambda5)", stringAuto(derivative[6]),
        stringAuto(energy[6] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Re(Lambda6)", stringAuto(derivative[7]),
        stringAuto(energy[7] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Im(Lambda6)", stringAuto(derivative[8]),
        stringAuto(energy[8] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Re(Lambda7)", stringAuto(derivative[9]),
        stringAuto(energy[9] - ef_start)});
    tab.add_row(std::vector<std::string>{
        "Im(Lambda7)", stringAuto(derivative[10]),
        stringAuto(energy[10] - ef_start)});
    tab.print();
    std::cout << "\n";
  }
};

/*----------------------------------------------------------------------------*/

// A structure that can be used to set a range for a double parameter.
struct double_range
{
  bool fixed;
  double fixedValue;
  double min, max;
  
  double_range() : fixed(false){}
  double_range(double min_in, double max_in) : fixed(false), min(min_in), max(max_in) {}
  double_range(double value_in) : fixed(true), fixedValue(value_in) {}

  double draw_random(const gsl_rng* rng) const
  {
    if ( fixed)
      return fixedValue;
    else 
      return min + (max - min) * gsl_rng_uniform(rng);;
  }

  // Overloading the << operator
  friend std::ofstream &operator<<(std::ofstream &of, const double_range &range)
  {
    if ( range.fixed)
      of << range.fixedValue;
    else
    of << "( " << range.min << ", " << range.max << ")";
    return of;
  }
  // Overloading the << operator
  friend std::ostream &operator<<(std::ostream &of, const double_range &range)
  {
    if (range.fixed)
      of << range.fixedValue;
      else
    of << "( " << range.min << ", " << range.max << ")";
    return of;
  }

  std::string toString()
  {

    std::string rangeString;

    if (fixed)
      rangeString = std::to_string(fixedValue);
    else
       rangeString =  "( " + std::to_string(min) + ", " + std::to_string(max) + " )";
    
    return rangeString;
  }
};


/*----------------------------------------------------------------------------*/