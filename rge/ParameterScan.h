/**=============================================================================
 * @brief: ParameterScan class
 * @author: Joel Oredsson
 *
 * Parameter scan class that can be used to scan the parameter space of a 2HDM
 *
 *============================================================================*/

#pragma once

#include "Structures.h"
// #include "THDM.h"
#include "THDM_complex.h"

#include <atomic>
#include <chrono>
#include <gsl/gsl_rng.h>
#include <iostream>
#include <mutex>
#include <thread>

/*******************************************************************************
 * @brief Types of scans
 *
 */
enum ScanType // Used when switching between different random scans
{
  EXACT_Z2,
  SOFTLY_BROKEN_Z2,
  HARD_BROKEN_Z2,
  MODEL_PROCESS,
  ALIGNED_YUKAWA,
  SOFTLY_BROKEN_Z2_ALIGNED,
  FLAT_Z2_CP_CONSERVED
};

/*******************************************************************************
 * @brief: Collection of parameter ranges for hybrid basis
 */
struct Range_hybrid
{
  double_range mh, mH, cba, tanb, Z4, Z5, Z7;
  std::string name;
  Z2symmetry yukawaType;

  Range_hybrid()
      : mh(105, 145), mH(150, 1000), cba(-0.5, 0.5), tanb(1.1, 50),
        Z4(-M_PI, M_PI), Z5(-M_PI, M_PI), Z7(-M_PI, M_PI), name("Default"),
        yukawaType(TYPE_I) {}

  void print() const
  {
    std::cout << name << "'s free parameter ranges:\n";
    std::cout << "mh: " << mh << std::endl;
    std::cout << "mH: " << mH << std::endl;
    std::cout << "cba: " << cba << std::endl;
    std::cout << "tanb: " << tanb << std::endl;
    std::cout << "Z4: " << Z4 << std::endl;
    std::cout << "Z5: " << Z5 << std::endl;
    std::cout << "Z7: " << Z7 << std::endl;
  }

  Base_hybrid get_random_point(const gsl_rng *rng) const
  {
    Base_hybrid hyb;

    // Random free parameters
    hyb.mh = mh.draw_random(rng);
    hyb.mH = mH.draw_random(rng);
    hyb.cba = cba.draw_random(rng);

    if (tanb.fixed)
      hyb.tanb = tanb.fixedValue;
    else
    {
      double_range betaRange(atan(tanb.min), atan(tanb.max));
      double beta = betaRange.draw_random(rng);
      hyb.tanb = tan(beta);
    }
    
    hyb.Z4 = Z4.draw_random(rng);
    hyb.Z5 = Z5.draw_random(rng);
    hyb.Z7 = Z7.draw_random(rng);

    return hyb;
  }
};

/**
 * @brief: Evolves a vector of THDM objects using multiple threads
 */
void evolve_thdm_vector(std::vector<THDM_complex> &thdmVec);
void thread_work_evolve( // std::atomic<int> &count,
    std::vector<THDM_complex> &thdmVec);

/*******************************************************************************
 * @brief Class that scans the parameter space of THDM
 *
 * It can perform the scans described by ScanType.
 *
 */
class ScanSystem
{
public:
  /**
   * @brief Constructor
   * Initializes all member variables.
   * Totalthreads determines the number of threads used when scanning the
   * parameter space.
   */
  ScanSystem(const int totalThreads = 1);
  ~ScanSystem();

  /**
   * @brief Set options
   *
   * Set-functions for RGE evolution options, number of points that should
   * be found and directory where to store the output data.
   */
  void set_rgeConfig(const RgeConfig &rgeConfig);
  void set_max_points(const int maxPoints);
  void set_directory(const std::string &directory);
  void set_scan_description(const std::string &description);
  void set_yukawa_type(Z2symmetry type);
  void set_minimum_energy(const double energy);
  void set_hybrid_basis_range(const Range_hybrid &hybRange);
  void set_base_generic(const Base_generic &gen);
  void set_spheno_config(const bool runSpheno, const int massLoopLvl);
  void use_minuit(const bool useMinuit);
  void create_model_files(const bool createModelFiles);

  /**
   * @brief Sets the ScanType
   *
   * Can choose between different scan scenarios.
   */
  void set_scanType(const ScanType &scanType);
  void set_aF_scan(const FermionSector &fermionSector,
                   const Z2symmetry &z2Symmetry);

  /**
   * @brief Analyses a THDM
   *
   * Scans the neighborhood of the parameter point of the
   * THDM.
   */
  void process_model(const THDM_complex &model);

  /**
   * @brief Evolves saved parameter points
   *
   * Evolves the parameter points saved in the directory "dir".
   */
  void process_folder(const std::string &dir);
  void process_folder_1loop(const std::string &dir);

  /**
   * @brief Evolve collection of THDM points with different level of
   *        Z2 breaking
   *
   * Argument dir is the directory, where parameterPoints_invariant.txt is
   * located. These parameter points should be Z_2 symmetric models.
   * This scan then breaks the symmetry by generating deviations for Z7, Z2
   * and Z3.
   *
   * Soft breaking: Random Z7, fixed Z2 and Z3 (but they change since they
   *                depend on Z7). dir should then point to an exact Z2 dataset.
   * Hard breaking: Random Z7, Z2, Z3. The folder should contain softly broken
   *                parameter points. dir should then point to a softly broken
   *                Z2 dataset.
   */
  void soft_z2_breaking(const std::string &dir);
  void hard_z2_breaking(const std::string &dir);

  void hard_scalar_breaking(const std::string &dir);

  /**
   * @brief Starts scan
   *
   * Creates datafiles for storing parameter points.
   * Sets up drivers for _rgeSystem (one for each thread).
   * Creates threads that each does thread_work().
   * Joins all threads when _maxPoints have been found.
   */
  void run_rge_scan();

private:
  /**
   * @brief Prints information to the console.
   */
  void print_time() const;
  void print_average_point_time() const;
  void print_status() const;
  void print_results() const;
  void print_scanType() const;
  void print_yukawaType() const;

  void create_parameter_info_file() const;

  gsl_rng *create_gsl_rng(const int seedNumber = 1) const;

  /**
   * @brief Loads parameters in file into a vector
   */
  std::vector<std::vector<double>>
  load_folders_base_invariant(const std::string &dir) const;

  /**
   * @brief: Load SLHA modelfiles
   *
   * @returns a vector of THDM_complex objects.
   */
  std::vector<THDM_complex> load_model_files(const std::string &dir) const;

  /**
   * @brief Evolves THDM objects
   *
   * Splits up a vector containing initialized THDMs into equal parts. A number
   * of multiple threads then evolve each part of the vector independently.
   * Useful when one wants to evolve a big number of parameter points.
   */
  void evolve_thdmVec(std::vector<THDM_complex> &thdmVec);

  /**
   * @brief Searches for good parameter points
   *
   * When the scan starts, each thread is constructed with this function. While
   * the scan is running, each thread is generating parameter points, checks
   * them and finally stores them if they satisfy the requirements.
   */
  void thread_work_scan(const int threadID);

  /**
   * @brief Evolves THDM objects
   *
   * The function process_model divides up the parameter space in the
   * neighborhood of a model and creates a vector of all the THDMs. Each thread
   * is then evolving a subset of this set of THDMs.
   */
  void thread_work_thdmVec(const int threadID,
                           std::vector<THDM_complex> thdmVec);

  /**
   * @brief Generates random parameter point
   *
   * Sets the parameters of a thdm randomly within the ranges set up for the
   * scan.
   */
  void generate_point(THDM_complex &thdm, const gsl_rng *rng) const;

  bool parameter_point_is_okay(THDM_complex &thdm) const;
  /**
   * @brief Check of thdm
   *
   * Whenever the scan has generated a point and processed it, this function is
   * called to check the model; which determines whether to save the parameter
   * point.
   *
   * @returns true if the thdm is deemed "good"; false otherwise.
   */
  bool satisfies_requirements(const THDM_complex &thdm) const;

  /**
   * @brief Creates data files
   *
   * Creates text files where "good" parameter points are saved when the scan is
   * running.
   *
   * Also create a file scanInfo.txt, that contains the configuration for the
   * parameter scan.
   */
  void create_data_files();

  /**
   * @brief Creates model file
   *
   * Creates a model file stored in directory/ModelFiles; which contains all
   * essential information about a THDM.
   */
  void create_model_file(const THDM_complex &thdm) const;

  void create_sm_modelFile(const SM_RGE &sm);

  /**
   * @brief Saves a thdm to files
   *
   * Everytime a thread finds a "good" point, it prints it to files. This
   * function is therefore wrapped with a mutex, since all threads are writing
   * to the same files.
   */
  void print_to_files(const THDM_complex &thdm, const THDM_complex &thdmEvolved,
                      const int threadID);

  void print_evolution_data(const THDM_complex &thdm, const std::string &file);
  void print_evolved_data(const THDM_complex &thdm, const std::string &file);
  void print_generic_basis(const THDM_complex &thdm, const std::string &file);

  void print_physical_data_files(const THDM_complex &thdm);

private:
  Range_hybrid _hybRange;

  ScanType _scanType;

  Base_generic _gen;            // Used for the ALIGNED_YUKAWA scan.
  FermionSector _fermionSector; // Used for ALIGNED_YUKAWA scan.
  Z2symmetry _z2Symmetry;

  double _minEnergy;

  bool _createModelfiles; // If true, creates SLHA files for all found points.
  bool _runSpheno;
  bool _useMinuit;  // if true, uses Minuit to improve parameter scan.
  int _massLoopLvl; // Sets the loop corrections for Higgs masses (0,1 or 2)

  FileSystem _files;
  std::string _scanDataFile, _scanDataFile_1loop, _scanDataFile_generic,
      _scanDataFile_generic_1loop_evolved, _scanDataFile_generic_evolved,
      _scanDataFile_higgs, _scanDataFile_invariant, _scanDataFile_physical,
      _scanDataFile_spheno, _scanDataFile_yukawa, _scanDataFile_evolved,
      _scanDataFile_1loop_evolved;
  std::string _description; // Description of scan that is printed to file.

  std::string _directory; // Directory for storing result data
  int _threads;           // Number of threads to run scan on.
  int _maxPoints;         // Number of parameter points that should be found.
  RgeConfig _rgeConfig;   // Options for RG running that evolves thdm objects.

  std::atomic<int>
      _pointsFound;            // Points found that satisfies satisfies_requirements()
  std::atomic<bool> _quitScan; // Scan quits if it is set to true.
  std::mutex _fileMutex;       // Mutex for access to _scanDataFiles
  std::mutex _sm_modelFileMutex;

  // Time when ScanSystem is created.
  std::chrono::time_point<std::chrono::high_resolution_clock> _startTime;

  // Squared VEV of Higgs doublets. Used when generating random parameters.
  double _v2;
};