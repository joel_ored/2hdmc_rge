#include "THDM_fitter.h"

#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnStrategy.h"

Base_generic fit_to_125mh(const THDM_complex &thdm, const int loopLvl)
{
  using namespace ROOT::Minuit2;

  Timer timer("Fit_to_125mh:");
  
  static const double chi2Limit = 10.; // Largest Chi2 for "good" fit.

  // Minuit settings
  static const unsigned int minuit_maxFcnCalls = 10;
  static const double minuit_tolerance = 100;
  static const unsigned int strategyLvl = 0;

  EwScaleFitter thdm_fitter;
  thdm_fitter.set_start_thdm(thdm);
  thdm_fitter.set_loop_level(loopLvl);

  Base_generic genStart = thdm.get_param_gen();
  MnUserParameters upar = create_minuit_user_parameters(genStart);
  
  MnMigrad migrad(thdm_fitter, upar, strategyLvl);
  // MnMigrad migrad(thdm_fitter, upar);
  // FunctionMinimum min = migrad(minuit_maxFcnCalls, minuit_tolerance);
  FunctionMinimum min = migrad();

  upar = min.UserParameters();
  Base_generic genFinal;
  // These parameters are not fitted by Minuit
  genFinal.beta = genStart.beta;
  genFinal.Lambda6 = genStart.Lambda6;
  genFinal.Lambda7 = genStart.Lambda7;
  genFinal.M112 = genStart.M112;
  genFinal.M222 = genStart.M222;
  retrieve_parameters_from_mup( genFinal, upar); // Retrieving fitted params.

  if (min.Fval() < chi2Limit)
    {
      std::cout << "\nFound a good fit!\n";
      std::cout << "Min chi2: " << min.Fval() << std::endl;
      std::cout << "Number of fcn calls: " << min.NFcn() << std::endl;
    }
    else {
      std::cout << "\nCouldn't find a good fit:(\n";
      std::cout << "Min chi2: " << min.Fval() << std::endl;
      std::cout << "Number of fcn calls: " << min.NFcn() << std::endl;
    }

  return genFinal;
}

Base_generic random_fitted_base_generic(THDM_complex &thdm, const int loopLvl,
                                        const gsl_rng *rng)
{
  // Algorithm:
  //  1.) Generate random softly broken Z2 scalar potential (using hybrid basis)
  //  2.) Setup EwScaleFitter and init with starting parameter point
  //  3.) Setup Minuit(thdm_fitter, start point) and try to minimize chi2.
  //  4.) If chi2is low enough, convert to base_generic otherwise start at 1
  //  again.

  using namespace ROOT::Minuit2;

  Timer timer("random_fitted_base_generic():");

  Base_generic genStart, genBest;
  Base_hybrid hybStart;

  EwScaleFitter thdm_fitter;
  thdm_fitter.set_start_thdm(thdm);
  thdm_fitter.set_loop_level(loopLvl);

  static const double chi2Limit = 10.;
  static const int maxTries = 1000;
  static const unsigned int minuit_maxFcnCalls = 100;
  static const double minuit_tolerance = 0.1;
  static const unsigned int strategyLvl = 0;

  int tries = 0;
  double minChi2 = 1.e20;

  for (;;)
  {
    hybStart.generate_random_softCpConserved(rng);
    genStart = hybStart.convert_to_generic(thdm.get_v2());

    if (!thdm_fitter.set_base_generic(genStart))
      continue; // Start point must be pert,unit,stab

    MnUserParameters upar = create_minuit_user_parameters(genStart);

    // Running Minuit
    MnStrategy minuitStrategy(strategyLvl);
    minuitStrategy.SetGradientNCycles(1);
    // std::cout << "Default cycles = " << minuitStrategy.GradientNCycles() <<
    // std::endl;
    MnMigrad migrad(thdm_fitter, upar, minuitStrategy);

    std::cout << "\n==================================================\n";
    std::cout << " Minimizing chi2 (try nr " << tries + 1 << ")...\n";
    std::cout << "==================================================\n";
    FunctionMinimum min = migrad(minuit_maxFcnCalls, minuit_tolerance);

    if (min.Fval() < minChi2)
    {
      // These parameters are not changed by Minuit
      genBest.beta = genStart.beta;
      genBest.Lambda6 = genStart.Lambda6;
      genBest.Lambda7 = genStart.Lambda7;

      upar = min.UserParameters();
      retrieve_parameters_from_mup(genBest, upar);
      minChi2 = min.Fval();
    }

    if (min.Fval() < chi2Limit)
    {
      std::cout << "\nFound good parameter point after " << ++tries
                << " tries!\n";
      std::cout << "Min chi2: " << min.Fval() << std::endl;
      std::cout << "Number of fcn calls: " << min.NFcn() << std::endl;
      break;
    }
    else if (++tries >= maxTries)
    {
      std::cout << "\nReached maximum tries for minimizing chi2 :(\n";
      break;
    }
  }
  return genBest;
}

void improve_parameters(THDM_complex &thdm)
{
  using namespace ROOT::Minuit2;

  Timer timer("improve_parameters:");

  Base_generic genStart = thdm.get_param_gen();

  HighScaleFitter thdm_fitter;
  thdm_fitter.set_start_thdm(thdm);

  // Setting up Minuit
  static const unsigned int minuit_maxFcnCalls = 100;
  static const double minuit_tolerance = 0.1;
  static const unsigned int strategyLvl = 0;
  static const unsigned int gradientCycles = 2;

  MnUserParameters upar = create_minuit_user_parameters(genStart);
  MnStrategy minuitStrategy(strategyLvl);
  minuitStrategy.SetGradientNCycles(gradientCycles);
  MnMigrad migrad(thdm_fitter, upar, minuitStrategy);

  std::cout << "\n==================================================\n";
  std::cout << " Minimizing chi2...\n";
  std::cout << "==================================================\n";
  FunctionMinimum min = migrad(minuit_maxFcnCalls, minuit_tolerance);

  upar = min.UserParameters();
  Base_generic genFinal;
  // These parameters are not changed by Minuit
  genFinal.beta = genStart.beta;
  genFinal.Lambda6 = genStart.Lambda6;
  genFinal.Lambda7 = genStart.Lambda7;
  retrieve_parameters_from_mup(genFinal, upar);
  thdm.set_param_gen( genFinal);

  std::cout << "\nMin chi2: " << min.Fval() << std::endl;
  std::cout << "Number of fcn calls: " << min.NFcn() << std::endl;
  std::cout << "Best fit: " << genFinal << std::endl;
}

//------------------------------------------------------------------------------

namespace ROOT
{

namespace Minuit2
{

MnUserParameters create_minuit_user_parameters(const Base_generic &gen)
{
  static const double step = 0.1;
  static const double minM12 = 100.;
  static const double maxM12 = 1.e6;
  static const double minLambda = -M_PI;
  static const double maxLambda = M_PI;

  MnUserParameters upar;
  upar.Add("M12", real(gen.M12), step, minM12, maxM12);
  upar.Add("Lambda1", gen.Lambda1, step, minLambda, maxLambda);
  upar.Add("Lambda2", gen.Lambda2, step, minLambda, maxLambda);
  upar.Add("Lambda3", gen.Lambda3, step, minLambda, maxLambda);
  upar.Add("Lambda4", gen.Lambda4, step, minLambda, maxLambda);
  upar.Add("Lambda5", real(gen.Lambda5), step, minLambda, maxLambda);

  return upar;
}

void retrieve_parameters_from_mup(Base_generic &gen,
                                  const MnUserParameters &mup)
{
  gen.M12 = std::complex<double>(mup.Value(0), 0.);
  gen.Lambda1 = mup.Value(1);
  gen.Lambda2 = mup.Value(2);
  gen.Lambda3 = mup.Value(3);
  gen.Lambda4 = mup.Value(4);
  gen.Lambda5 = std::complex<double>(mup.Value(5), 0.);
}

//------------------------------------------------------------------------------

EwScaleFitter::EwScaleFitter() : _massLoopLvl(0) { init(); }

EwScaleFitter::~EwScaleFitter()
{
  // std::cout << "[DEBUG]: Destroying EwScaleFitter!\n";
}

double EwScaleFitter::operator()(const std::vector<double> &par) const
{
  // Timer timer("EwScaleFitter operator():");

  THDM_complex thdm = _thdm;

  Base_generic gen = _thdm.get_param_gen();
  gen.M12 = std::complex<double>(par[0], 0.);
  gen.Lambda1 = par[1];
  gen.Lambda2 = par[2];
  gen.Lambda3 = par[3];
  gen.Lambda4 = par[4];
  gen.Lambda5 = std::complex<double>(par[5], 0.);

  thdm.set_param_gen(gen, false);

  // Evolve the thdm to the EW scale to compute mh
  if (thdm.get_renormalization_scale() != 173.34)
  {
    thdm.set_final_energy_scale(173.34);
    // thdm.print_potential();
    thdm.evolve();
    // thdm.print_rgeResults();
  }

  // Calculates loop corrected Higgs masses using SPheno
  if (_massLoopLvl > 0)
    thdm.run_spheno(_massLoopLvl);

  return chi2(thdm);
}

double EwScaleFitter::Up() const { return 1.; }

void EwScaleFitter::init()
{
  _obs.clear();

  _obs.emplace_back(125., 5., 5.);      // SM like Higgs at EW scale
  _obs.emplace_back(173.34, 50., 1.e4); // The renormalization scale 173.34 GeV
}

void EwScaleFitter::set_start_thdm(const THDM_complex &thdm)
{

  RgeConfig rgeConfig = thdm.get_rgeConfig();
  rgeConfig.dataOutput = false;
  rgeConfig.consoleOutput = false;
  rgeConfig.unitarity = true;
  rgeConfig.stability = true;

  _thdm.set_from_thdm(thdm);
  _thdm.set_logLevel(LOG_WARNINGS);
  _thdm.set_rgeConfig(rgeConfig);
}

bool EwScaleFitter::set_base_generic(const Base_generic &gen)
{
  if (!_thdm.set_param_gen(gen))
    return false;
  if (!_thdm.is_pert_unit_stab())
    return false;
  return true;
}

void EwScaleFitter::set_loop_level(const int loopLvl)
{
  _massLoopLvl = loopLvl;
}

double EwScaleFitter::chi2(const THDM_complex &thdm) const
{
  double mh;

  if (_massLoopLvl > 0)
    mh = thdm.get_spheno_output()[0];
  else
    mh = thdm.get_higgs_treeLvl_masses()[0];

  // chi2 penalty if mh is unphysical
  if (std::isnan(mh))
    mh = 1.e10;

  std::vector<double> obsFit = {mh, thdm.get_renormalization_scale()};

  double chi = 0.;

  for (unsigned int i = 0; i < obsFit.size(); ++i)
  {
    chi += _obs[i].chiContrib(obsFit[i]);
  }

  // Printing progress to console
  std::cout << "\r"
            << " Chi2: ";
  std::cout.width(12);
  std::cout << chi << ", mu = ";
  std::cout.width(12);
  std::cout << thdm.get_renormalization_scale() << " GeV, mh = ";
  std::cout.width(12);
  std::cout << obsFit[0] << " GeV";
  std::cout.width(20);
  std::cout << std::flush;

  return chi;
}

//------------------------------------------------------------------------------

HighScaleFitter::HighScaleFitter() { init(); }
HighScaleFitter::~HighScaleFitter() {}

double HighScaleFitter::operator()(const std::vector<double> &par) const
{
  // Timer timer("HighScaleFitter operator():");

  THDM_complex thdm = _thdm;

  Base_generic gen = _thdm.get_param_gen();
  gen.M12 = std::complex<double>(par[0], 0.);
  gen.Lambda1 = par[1];
  gen.Lambda2 = par[2];
  gen.Lambda3 = par[3];
  gen.Lambda4 = par[4];
  gen.Lambda5 = std::complex<double>(par[5], 0.);

  thdm.set_param_gen(gen);

  thdm.evolve();

  double chiSquared = chi2(thdm);

  // if( chiSquared < 10.)
  //   return 0.;
  // else 
    return chiSquared;
  // return chi2(thdm);
}

double HighScaleFitter::Up() const { return 1.; }

void HighScaleFitter::set_start_thdm(const THDM_complex &thdm)
{

  RgeConfig rgeConfig = thdm.get_rgeConfig();
  rgeConfig.dataOutput = false;
  rgeConfig.consoleOutput = false;
  rgeConfig.perturbativity = true;
  rgeConfig.unitarity = true;
  rgeConfig.stability = true;

  set_highScale(rgeConfig.finalEnergyScale);

  _thdm.set_from_thdm(thdm);
  _thdm.set_logLevel(LOG_WARNINGS);
  _thdm.set_rgeConfig(rgeConfig);
}


void HighScaleFitter::init()
{
  _obs.clear();

  _obs.emplace_back(1.e18, 1.e17, 1.e19);
}

void HighScaleFitter::set_highScale(const double highScale)
{
  _obs.clear();

  _obs.emplace_back(highScale, 0.1 * highScale, 10. * highScale);
}

double HighScaleFitter::chi2(const THDM_complex &thdm) const
{

  std::vector<double> obsFit = {thdm.get_renormalization_scale()};

  double chi2 = 0.;

  for (unsigned int i = 0; i < obsFit.size(); ++i)
  {
    chi2 += _obs[i].chiContrib(obsFit[i]);
  }

  // Printing progress to console
  std::cout << "\r"
            << " Chi2: ";
  std::cout.width(12);
  std::cout << chi2 << ", mu = ";
  std::cout.width(12);
  std::cout << thdm.get_renormalization_scale() << " GeV";
  std::cout.width(12);
  std::cout << std::flush;

  return chi2;
}

} // namespace Minuit2
} // namespace ROOT