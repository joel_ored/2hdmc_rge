/**
 * GUI (Graphical User Interface) for THDM_complex
 * 
 * A small GUI that enables the user to easily edit a THDM_complex
 * object in the Higgs or Generic basis. RGE evolution can be performed
 * and configured as one wish. There is also the possibility to show 
 * plots of the evolution.
 * 
 * There is also a function that can calculate the gradient of the 
 * breakdown energy landscape.
 * 
 * Dependencies:
 * The GUI is buildt with the library gtkmm-3.0.
 * Compile with the c++ flag `pkg-config gtkmm-3.0 --cflags --libs`
 */
#pragma once

#include "THDM_complex.h"
// #include "RGE_system.h"
#include "Structures.h"
#include "ParameterScan.h"

#include <gtkmm.h>
#include <vector>
#include <complex>
#include <thread>
#include <Eigen/Dense>

/**
 * @brief 3*3 complex matrix GUI.
 * 
 * A GUI to translate 3*3 numerical entries to a Eigen::Matrix3cd
 */
class MatrixGUI
{
public:
  MatrixGUI(const std::string &name);
  ~MatrixGUI();

  /* Fixes the _elements from a builder. */
  void setEntries(Glib::RefPtr<Gtk::Builder> builder);

  /* Reads in the text entries in _elements, updates _matrix 
          and returns it. */
  Eigen::Matrix3cd read();

  /* Sets _matrix and updates the entries to the corresponding 
          values. */
  void set(const Eigen::Matrix3cd &matrix);

private:
  /** 
         * @params:
         *  _name: Name of matrix.
         *  _matrix: 3*3 complex matrix.
         *  _elements: 3*3 text entries.
         */
  std::string _name;
  Eigen::Matrix3cd _matrix;
  std::vector<Gtk::Entry *> _elements;

  /* Sets entries to the values of _matrix. */
  void update();

  /* Translates an entry to a complex number. */
  std::complex<double> entryToComplex(Glib::ustring entry) const;
};

/*------------------------------------------------------------------------*/

/** Class for a THDM GUI window that can be used to modify
 * a THDM_complex object. Output can be displayed in the
 * terminal.
 */
class GUI : public Gtk::Window
{

public:
  GUI();
  virtual ~GUI();

protected:
  /* Functions that are being called by clicking buttons. */
  void button_printAll();       // prints all the parameters of _thdm to the console
  void button_printPotential(); // prints the potential and masses of _thdm to the consol
  void button_close();          // Closes the window.
  void button_evolve();         // Performs RGE evolution with the set RGE options and basis parameters
  void button_set();            // Updates the _thdm parameters from the _spinbuttons
  void button_load();           // Opens dialog that's used to open a model file.

  /** Calculates the gradient of the energy landscape
     * and prints it out to the consol. */
  void button_calcGradient();

  /** Creates a thread that runs runAlongGradient function.
     * 
     * It walks upwards the gradient in the energy landscape,
     * so to find parameter points where it breaks requirements
     * at higher and higher scales.
     *   
     * After toggling the _buttonFindMaximum, the scan is
     * being stopped at next step, the thread joins and
     * the _thdm is updated to the last point in the scan. 
     **/
  void button_findMax();

  /** Functions that show plots of the evolution 
     * of certain parameters with the help of gnuplot. */
  void button_plot_lambda() const;
  void button_plot_Mij() const;
  void button_plot_tanb() const;
  void button_plot_masses() const;
  void button_plot_yukawa(const int matrixID) const;

  /* Each button, box etc. is stored as a Gtk-object pointer. */
  Glib::RefPtr<Gtk::Builder> _builder;
  Gtk::Grid *_grid;
  Gtk::Notebook *_notebookPotential;
  Gtk::Notebook *_notebookYukawa;
  Gtk::ComboBox *_comboBoxBase;
  Gtk::ComboBox *_comboBoxZ2symmetry;

  Gtk::Button *_buttonClose;
  Gtk::Button *_buttonPrintAll;
  Gtk::Button *_buttonPrintPotential;
  Gtk::Button *_buttonEvolve;
  Gtk::Button *_buttonSet;
  Gtk::Button *_buttonLoad;

  Gtk::Button *_buttonCalcGradient;
  Gtk::ToggleButton *_buttonFindMaximum;

  Gtk::Button *_buttonPlotLambda;
  Gtk::Button *_buttonPlotMij;
  Gtk::Button *_buttonPlotTanb;
  Gtk::Button *_buttonPlotMasses;

  std::vector<Gtk::Button *> _buttonsPlotYukawa;
  // std::vector<Gtk::Button*> _buttonsPlotYukawaGeneric;

  /* Buttons for choosing RGE evolution options. */
  Gtk::Entry *_entryEvolutionName;
  Gtk::ToggleButton *_buttonDataOutput;
  Gtk::ToggleButton *_buttonConsoleOutput;
  Gtk::ToggleButton *_buttonTwoLoop;
  Gtk::ToggleButton *_buttonPerturbativity;
  Gtk::ToggleButton *_buttonUnitarity;
  Gtk::ToggleButton *_buttonStability;
  Gtk::SpinButton *_buttonOdeSteps;

  /**
     * @brief Pointers to spinbuttons.
     * 
     * The spinbuttons are used when setting the numerical values of 
     * parameters of the THDM_complex object. 
     * 
     * initializeSpinbuttons() sets up the pointers.
     */
  std::vector<Gtk::SpinButton *> _spinbuttons;
  void initializeSpinbuttons();

  /* Matrices. */
  MatrixGUI _kU, _kD, _kL, _rU, _rD, _rL;
  MatrixGUI _eta1U, _eta1D, _eta1L, _eta2U, _eta2D, _eta2L;
  std::vector<std::string> _matrixNames = {"kU", "kD", "kL", "rU", "rD", "rL",
                                           "eta1U", "eta1D", "eta1L", "eta2U",
                                           "eta2D", "eta2L"};

private:
  /* THDM related members */

  /* Sets the _thdm parameters from the _spinbuttons. */
  void init_THDM();

  /** Sets up _scan to walk a THDM_complex along the 
     * energy gradient. */
  void runAlongGradient();

  /** Reads off the buttons that sets the RGE evolution
     * options. */
  void read_RGE_options();

  /** Updates the basis choice. */
  void change_basis();

  /* Updates the GUI from the _thdm's values. */
  void updateGuiFromThdm();

  /* Updates the _thdm from the GUI's entries. */
  void updateThdmFromGui();

  FileSystem _fileSystem;

  THDM_complex _thdm;
  BaseType _currentBasis;
  // Base_higgs _higgs;
  // Base_generic _generic;
  // Base_hybrid _hybrid;

  RgeConfig _rgeConfig;
  Gradient _gradient;
  // ScanSystem _scan;

  /** The runAlongGradient function is very timeconsuming
     * and is therefore separated onto its own thread.
     * After toggling the _buttonFindMaximum, the scan is
     * being stopped at next step, the thread joins and
     * the _thdm is updated to the last point in the scan. */
  std::thread _thread;
};
