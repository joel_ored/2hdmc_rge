#include "SM_RGE.h"
#include "HelpFunctions.h"
#include "RGE.h"
#include <complex>
#include <string>
#include <vector>

/**
 *   The standard model is created with the ms-bar parameters at the scale 
 * MT = 173.34 GeV. 
 *   Fermion masses are taken from arXiv:0712.1419. 
 *   The Higgs coupling is taken from arXiv:1307.3536v4. Used the tree-lvl 
 * relation v = sqrt(2) mt / yt = (sqrt(2) 162.2 / 0.93690) GeV (approx 245 GeV) 
 * to compute a value for the VEV at MT. 
 *   Gauge couplings are also from arXiv:1307.3536v4.
 * TODO: Is there a big difference to use Vev of minimized renormalized 
 * TODO: potential?  
 */
SM_RGE::SM_RGE()
    : _g1(0.35830),
      _g2(0.64779),
      _g3(1.1666),
      _lambda(0.12604),
      _v2(59943.9)
{
  _modelName = "SM";
  _renormScale = 173.34;

  _rgeConfig.dataOutput = false;
  _rgeConfig.consoleOutput = false;
  _rgeConfig.evolutionName = "SM";
  _rgeConfig.twoloop = true;
  _rgeConfig.perturbativity = true;
  _rgeConfig.stability = false;
  _rgeConfig.unitarity = false;
  _rgeConfig.finalEnergyScale = 500.;
  _rgeConfig.steps = 10;
  _console.set_logLevel(LOG_WARNINGS);

  // The RGE running of the SM is performed with full 2-loop RGEs with 6 quark
  // flavors. 59 is the number of params in y[].
  init_rge_systems(rgeFuncSm_1loop, rgeFuncSm_2loop, 59); 

  init_yukawa_sector();
}

SM_RGE::~SM_RGE() {
  // std::cout << "Destroying SM_RGE!\n";
}

bool SM_RGE::evolve_to(const double mu)
{
  _rgeConfig.finalEnergyScale = mu;
  return evolve();
}

bool SM_RGE::reset(const double y[], const double &renormScale) 
{
  set_model_from_y(y);
  _renormScale = renormScale;
  return true;
}

bool SM_RGE::reset_to_saved_state() { return true; }

void SM_RGE::print_all() const
{
  std::cout << "\nSM parameters:\n";
  std::cout <<"Renormalization scale: " << _renormScale
            << " GeV:\n";
 
  double thetaW = atan(_g1 / _g2);
  double cW = cos(thetaW);
  double alpha = (_g1 * cW) * (_g1 * cW) / (4. * M_PI);
  double aS = _g3 * _g3 / (4. * M_PI);
  Table tab1(4);
  tab1.set_frame_style("-", "|");
  tab1.set_frame_color(FrameColor::YELLOW);
  tab1.set_title("Gauge couplings");
  tab1.add_row(std::vector<std::string>{"g1:", stringAuto(_g1),
                                        "alpha^-1:", stringAuto(1. / alpha)});
  tab1.add_row(std::vector<std::string>{"g2:", stringAuto(_g2),
                                        "cosW:", stringAuto(cW)});
  tab1.add_row(std::vector<std::string>{"g3:", stringAuto(_g3),
                                        "alpha_S:", stringAuto(aS)});
  tab1.print();

  double v = sqrt(_v2);
  double mh = v * sqrt(2. * _lambda);
  double mu = mh / sqrt(2.);

  Table tab3(3);
  tab3.set_frame_style("-", "|");
  tab3.set_frame_color(FrameColor::GREEN);
  tab3.set_title("Higgs");
  tab3.add_row(std::vector<std::string>{"mh:", stringAuto(mh), "GeV"});
  tab3.add_row(std::vector<std::string>{"v:", stringAuto(v), "GeV"});
  tab3.add_row(std::vector<std::string>{"mu:", stringAuto(mu), "GeV"});
  tab3.add_row(std::vector<std::string>{"lambda:", stringAuto(_lambda), ""});
  tab3.print();

  Table tab2(2);
  tab2.set_frame_style("-", "|");
  tab3.set_title("Fermions");
  tab2.add_row(std::vector<std::string>{"Masses", "[GeV]"}, true);
  tab2.add_row(std::vector<std::string>{"mT:", stringAuto(_mup[2])});
  tab2.add_row(std::vector<std::string>{"mC:", stringAuto(_mup[1])});
  tab2.add_row(std::vector<std::string>{"mU:", stringAuto(_mup[0])});
  tab2.add_row(std::vector<std::string>{"mB:", stringAuto(_mdn[2])});
  tab2.add_row(std::vector<std::string>{"mS:", stringAuto(_mdn[1])});
  tab2.add_row(std::vector<std::string>{"mD:", stringAuto(_mdn[0])});
  tab2.add_row(std::vector<std::string>{"mTau:", stringAuto(_ml[2])});
  tab2.add_row(std::vector<std::string>{"mMu:", stringAuto(_ml[1])});
  tab2.add_row(std::vector<std::string>{"mE:", stringAuto(_ml[0])});
  tab2.print();

  std::cout << "VCKM:\n"
            << _VCKM << std::endl;
  std::cout << "\n";
}

bool SM_RGE::rge_update(const double y[], const double ti)
{
  _rgeResults.ef = exp(ti);
  _renormScale = _rgeResults.ef;
  set_model_from_y(y);
  return true;
}

void SM_RGE::rge_finished() { calc_fermionMasses_and_ckm(); }

void SM_RGE::calc_fermionMasses_and_ckm()
{
  // Using biunitary transformation from 2HDM case, where there are 6 Yukawa
  // matrices. Same procedure for SM but three of them are zero.
  Eigen::Matrix3cd tempMatrix;
  tempMatrix.setZero();

  BiUnitary(_yU, _yD, _yL, tempMatrix, tempMatrix, tempMatrix, _VCKM, _mup,
            _mdn, _ml, _v2);
  // Set the Yukawa matrices in the flavor basis again.
  _yD = _VCKM * _yD;
}

void SM_RGE::set_y(double y[]) const
{
  y[0] = _g1;
  y[1] = _g2;
  y[2] = _g3;
  y[3] = sqrt(_v2);
  y[4] = 2. * _lambda; // Different factor of 2 convention compared to 2HDM

  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
    {
      y[5 + i * 3 + j] = real(_yU(i, j));
      y[14 + i * 3 + j] = imag(_yU(i, j));
      y[23 + i * 3 + j] = real(_yD(i, j));
      y[32 + i * 3 + j] = imag(_yD(i, j));
      y[41 + i * 3 + j] = real(_yL(i, j));
      y[50 + i * 3 + j] = imag(_yL(i, j));
    }
}

void SM_RGE::set_model_from_y(const double y[])
{
  _g1 = y[0];
  _g2 = y[1];
  _g3 = y[2];
  _v2 = y[3] * y[3];
  _lambda = y[4] / 2.; // Different factor of 2 convention compared to 2HDM

  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
    {
      _yU(i, j) = std::complex<double>(y[5 + i * 3 + j], y[14 + i * 3 + j]);
      _yD(i, j) = std::complex<double>(y[23 + i * 3 + j], y[32 + i * 3 + j]);
      _yL(i, j) = std::complex<double>(y[41 + i * 3 + j], y[50 + i * 3 + j]);
    }
}

bool SM_RGE::is_perturbative() const { return true; }

bool SM_RGE::is_unitary() const { return true; }

bool SM_RGE::is_stable() const
{
  if (_lambda < 0)
    return false;

  return true;
}

void SM_RGE::set_ckm_from_pdg()
{
  // using namespace std;
  _VCKM.setZero();

  Eigen::Matrix3cd CKM_input;
  CKM_input.setZero();

  // Parameterisation of CKM matrix from PDG 2018.
  double lambda = 0.22453;
  double A = 0.836;
  double rhobar = 0.122;
  double etabar = 0.355;

  double s12 = lambda;
  double s23 = A * lambda * lambda;
  double c12 = sqrt(1. - s12 * s12);
  double c23 = sqrt(1. - s23 * s23);
  double preFactor =
      (A * pow(lambda, 3) * sqrt(1. - A * A * pow(lambda, 4))) /
      ((1. - lambda * lambda) *
       (1. - 2. * A * A * pow(lambda, 4) * rhobar +
        pow(A, 4) * pow(lambda, 8) * (pow(rhobar, 2) + pow(etabar, 2))));
  std::complex<double> s13eid = std::complex<double>(
      preFactor *
          (rhobar - A * A * pow(lambda, 4) * (pow(rhobar, 2) + pow(etabar, 2))),
      preFactor * etabar);
  double c13 = std::sqrt(std::abs(1. - std::abs(s13eid) * std::abs(s13eid)));

  CKM_input(0, 0) = std::complex<double>(c12 * c13, 0.);
  CKM_input(0, 1) = std::complex<double>(s12 * c13, 0.);
  CKM_input(0, 2) = conj(s13eid);
  CKM_input(1, 0) = std::complex<double>(-s12 * c23 - c12 * s23 * real(s13eid),
                                         -c12 * s23 * imag(s13eid));
  CKM_input(1, 1) = std::complex<double>(c12 * c23 - s12 * s23 * real(s13eid),
                                         -s12 * s23 * imag(s13eid));
  CKM_input(1, 2) = std::complex<double>(s23 * c13, 0.);
  CKM_input(2, 0) = std::complex<double>(s12 * s23 - c12 * c23 * real(s13eid),
                                         -c12 * c23 * imag(s13eid));
  CKM_input(2, 1) = std::complex<double>(-c12 * s23 - s12 * c23 * real(s13eid),
                                         -s12 * c23 * imag(s13eid));
  CKM_input(2, 2) = std::complex<double>(c23 * c13, 0.);

  _VCKM = CKM_input;
}

void SM_RGE::init_yukawa_sector()
{
  set_ckm_from_pdg();

  _yU.setZero();
  _yD.setZero();
  _yL.setZero();

  // Using the MS-bar fermion masses at scale Mt from arXiv:0712.1419.
  const static double mup[] = {0.00122, 0.590, 162.2};
  const static double mdn[] = {0.00276, 0.052, 2.75};
  const static double ml[] = {0.000485289396, 0.1024673155, 1.74215};

  for (int i = 0; i < 3; i++)
  {
    _mup[i] = mup[i];
    _mdn[i] = mdn[i];
    _ml[i] = ml[i];
    _yU(i, i) = std::complex<double>(mup[i] * sqrt(2. / _v2), 0.);
    _yD(i, i) = std::complex<double>(mdn[i] * sqrt(2. / _v2), 0.);
    _yL(i, i) = std::complex<double>(ml[i] * sqrt(2. / _v2), 0.);
  }

  // Going to the flavor eigenbasis
  _yD = _VCKM * _yD;
}

double SM_RGE::get_v2() const { return _v2; }

std::vector<double> SM_RGE::get_gauge_couplings() const
{
  return std::vector<double>{_g1, _g2, _g3};
}

std::vector<double> SM_RGE::get_mup() const
{
  return std::vector<double>{_mup[0], _mup[1], _mup[2]};
}

std::vector<double> SM_RGE::get_mdn() const
{
  return std::vector<double>{_mdn[0], _mdn[1], _mdn[2]};
}

std::vector<double> SM_RGE::get_ml() const
{
  return std::vector<double>{_ml[0], _ml[1], _ml[2]};
}

Eigen::Matrix3cd SM_RGE::get_vCkm() const
{
  return _VCKM;
}

double SM_RGE::get_lambda() const
{
  return _lambda;
}