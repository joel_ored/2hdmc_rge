/**=============================================================================
 * @brief:: Miscellaneous functions related to THDM
 * @author: Joel Oredsson
 *
 * Collection of different bases for THDM as structs.
 * Also other stuff...
 * 
 * Bases of general THDM:
 *   -> Generic
 *   -> Compact
 *   -> Higgs
 *   -> Invariant
 * 
 * Basis of CP conserving softly Z2 broken THDM:
 *   -> Hybrid
 *
 *============================================================================*/
#pragma once

#include <gsl/gsl_rng.h>
#include <fstream>
#include <iostream>
#include <complex>
#include <string>
#include <Eigen/Dense>

/**
 * @brief Different type of Z_2 symmetries of the THDMs Yukawa sector.
 */
enum Z2symmetry { NO_SYMMETRY, TYPE_I, TYPE_II, TYPE_III, TYPE_IV };

/**
 * @brief Bases that are implemented in the general complex 2HDM.
 */
enum BaseType { GENERIC, COMPACT, HIGGS, HYBRID, INVARIANT };

enum FermionSector
{
  UP, DOWN, LEPTON
};

//--THDM-bases------------------------------------------------------------------

/**
 * @brief Base struct for all bases.
 *
 * Base struct for all THDM_complex bases. They are all characterized by
 * different BaseType.
 *
 * @params:
 *   xi: phase of VEV.
 *   beta: angle related to generic basis with tan(beta) = v_2 / v_1.
 */
struct ThdmBasis {
  BaseType type;
  double xi, beta;
};

struct Base_generic;
struct Base_compact;
struct Base_higgs;
struct Base_invariant;
struct Base_hybrid;

/**
 * @brief: Generic 2HDM potential
 *
 * tanb is defined as the ratio of the Higgs doublets in this basis.
 */
struct Base_generic : ThdmBasis {
  double M112, M222, Lambda1, Lambda2, Lambda3, Lambda4;
  std::complex<double> M12, Lambda5, Lambda6, Lambda7;
  Eigen::Matrix3cd eta1U, eta2U, eta1D, eta2D, eta1L, eta2L;

  Base_generic() { type = GENERIC; xi = 0.; }

  // Overloading the << operator for ostream
  friend std::ostream &operator<<(std::ostream &oS, const Base_generic &gen) {
    oS << "Generic basis:\n"
       << "beta = " << gen.beta << ", tanb = " << tan(gen.beta) << std::endl
       << "M112 = " << gen.M112 << std::endl
       << "M222 = " << gen.M222 << std::endl
       << "M12 = " << gen.M12 << std::endl
       << "Lambda1 = " << gen.Lambda1 << std::endl
       << "Lambda2 = " << gen.Lambda2 << std::endl
       << "Lambda3 = " << gen.Lambda3 << std::endl
       << "Lambda4 = " << gen.Lambda4 << std::endl
       << "Lambda5 = " << gen.Lambda5 << std::endl
       << "Lambda6 = " << gen.Lambda6 << std::endl
       << "Lambda7 = " << gen.Lambda7 << std::endl;
     return oS;
  }
  
  void generate_random_softCpConserved(const gsl_rng *rng);

  Base_compact convert_to_compact() const;
  Base_higgs convert_to_higgs() const;
  Base_invariant convert_to_invariant(const double &v2) const;

  std::vector<double> convert_to_vector() const;
};

/**
 * @brief: Compact basis/notation for the generic potential
 */
struct Base_compact : ThdmBasis {
  std::complex<double> Y[2][2], Z[2][2][2][2];

  Base_compact() { type = COMPACT; xi = 0.; }

  // Overloading the << operator for ostream
  friend std::ostream &operator<<(std::ostream &oS, const Base_compact &comp) {
    oS << "Compact basis:\n";
       std::cout << "tanb = " << tan(comp.beta) << std::endl;
       std::cout << "Y_11 = " << comp.Y[0][0] << std::endl;
       std::cout << "Y_12 = " << comp.Y[0][1] << std::endl;
       std::cout << "Y_21 = " << comp.Y[1][0] << std::endl;
       std::cout << "Y_22 = " << comp.Y[1][1] << std::endl; 
       std::cout << "\nZ_1111 = " << comp.Z[0][0][0][0] << std::endl;
       std::cout << "Z_2222 = " << comp.Z[1][1][1][1] << std::endl;
       std::cout << "Z_1122 = Z_2211 = " << comp.Z[0][0][1][1] << std::endl;
       std::cout << "Z_1221 = Z_2112 = " << comp.Z[0][1][1][0] << std::endl;
       std::cout << "Z_1212 = " << comp.Z[0][1][0][1] << std::endl;
       std::cout << "Z_2121 = " << comp.Z[1][0][1][0] << std::endl;
       std::cout << "Z_1112 = Z_1211 = " << comp.Z[0][0][0][1] << std::endl;
       std::cout << "Z_1121 = Z_2111 = " << comp.Z[0][0][1][0] << std::endl;
       std::cout << "Z_2212 = Z_1222 = " << comp.Z[0][1][1][1] << std::endl;
       std::cout << "Z_2221 = Z_2122 = " << comp.Z[1][1][1][0] << std::endl;
     return oS;
  }
};

/**
 * @brief: Higgs basis
 *
 * The Higgs basis is defined as the basis where only the first Higgs doublet
 * acquires a VEV. It is unique, up to a phase transformation of the second
 * Higgs.
 *
 * @params:
 *  kU, kD, kL: diagonal Yukawa matrices proportional to the fermion
 *              mass matrices.
 *  rU, rD, rL: Arbitrary complex matrices. If a Z2 symmetry is imposed,
 *              these matrices are proportional to the corresponding
 *              kF matrices.
 */
struct Base_higgs : ThdmBasis {
  double mHc, Y1, Y2, Z1, Z2, Z3, Z4;
  std::complex<double> Y3, Z5, Z6, Z7;
  Eigen::Matrix3cd kU, rU, kD, rD, kL, rL;

  Base_higgs() { type = HIGGS; xi = 0.; }

  // Overloading the << operator for ostream
  friend std::ostream &operator<<(std::ostream &oS, const Base_higgs &higgs) {
    oS << "Higgs basis:\n"
       << "Y1 = " << higgs.Y1 << std::endl
       << "Y2 = " << higgs.Y2 << std::endl
       << "Y3 = " << higgs.Y3 << std::endl
       << "Z1 = " << higgs.Z1 << std::endl
       << "Z2 = " << higgs.Z2 << std::endl
       << "Z3 = " << higgs.Z3 << std::endl
       << "Z4 = " << higgs.Z4 << std::endl
       << "Z5 = " << higgs.Z5 << std::endl
       << "Z6 = " << higgs.Z6 << std::endl
       << "Z7 = " << higgs.Z7 << std::endl;
     return oS;
  }

  Base_generic convert_to_generic() const;
  Base_compact convert_to_compact() const;
  Base_invariant convert_to_invariant(const double &v2) const;

  std::vector<double> convert_to_vector() const;
};

/**
 * @brief: U(2) Higgs flavor invariant basis
 *
 * This basis is specified by the eigenvalues of the Higgs mass matrix;
 * combined with scalar mixing angles and some quartic couplings.
 * The rotation to the Higgs basis is specified in Phys.Rev.D 74, 015018.
 *
 * @params:
 *   The Yukawa sector is the same as in Base_Higgs. TODO NOT TRUE
 *   mh[3]: Neutral Higgs tree-lvl masses (Different ordering corresponds to
 *          different potential parameters)
 *   s12, c13: Sin/Cos of angles of a SO(3) matrix that diagonalizes the neutral
 *             Higgs mass matrix, range = -pi/2 to pi/2.
 *   mHc: Charged Higgs mass.
 *   Z2, Z3: Quartic couplings in the Higgs basis.
 *   Z7inv: Invariant combination Z7*exp(-itheta23).
 * 
 *   theta23: This parameter is arbitrary, corresponds to the shift chi
 *            in the Higgs basis. Setting it, only gives different Higgs bases.
 */
struct Base_invariant : ThdmBasis {
  double mHc, mh[3], s12, c13, Z2, Z3, theta23;
  std::complex<double> Z7inv;
  Eigen::Matrix3cd kU, rU, kD, rD, kL, rL;

  double cPhi; // cPhi is not needed as a free parameter, but is an U(2)
               // invariant one.

  Base_invariant() : theta23(0.) { type = INVARIANT; xi = 0.; }

  // Overloading the << operator for ostream
  friend std::ostream &operator<<(std::ostream &oS, const Base_invariant &inv) {
    oS << "Invariant basis (theta23 = " << inv.theta23 << " ):\n"
       << "mh[0] = " << inv.mh[0] << std::endl
       << "mh[1] = " << inv.mh[1] << std::endl
       << "mh[2] = " << inv.mh[2] << std::endl
       << "mHc = " << inv.mHc << std::endl
       << "Z2 = " << inv.Z2 << std::endl
       << "Z3 = " << inv.Z3 << std::endl
       << "Z7inv =" << inv.Z7inv << std::endl
       << "s12 = " << inv.s12 << std::endl
       << "c13 = " << inv.c13 << std::endl
       << "tanb = " << tan(inv.beta) << std::endl;
    return oS;
  }

  // Overloading the << operator for ofstream
  friend std::ofstream &operator<<(std::ofstream &of,
                                   const Base_invariant &inv) {
    of << "Invariant basis (theta23 = " << inv.theta23 << " ):\n"
       << "mh[0] = " << inv.mh[0] << std::endl
       << "mh[1] = " << inv.mh[1] << std::endl
       << "mh[2] = " << inv.mh[2] << std::endl
       << "mHc = " << inv.mHc << std::endl
       << "Z2 = " << inv.Z2 << std::endl
       << "Z3 = " << inv.Z3 << std::endl
       << "Z7inv =" << inv.Z7inv << std::endl
       << "s12 = " << inv.s12 << std::endl
       << "c13 = " << inv.c13 << std::endl
       << "tanb = " << tan(inv.beta) << std::endl;
    return of;
  }

  Base_generic convert_to_generic(const double &v2) const;
  Base_compact convert_to_compact(const double &v2) const;
  Base_higgs convert_to_higgs(const double &v2) const;

  std::vector<double> convert_to_vector() const;
};

/**
 * @brief: Hybrid basis
 *
 * Soflty broken Z_2 CP conserving hybrid basis from arxiv:1507.04281.
 * 
 * It is a combination of tree-lvl masses and Higgs basis quartic couplings.
 * Compared to the invariant basis, it is convenient to substitute mA and mHc
 * for Z4 and Z5; because it is easier to find parameter points with small
 * quartic couplings.
 * 
 * CP conservation is assumed, thus all parameters are real.
 * Degrees of freedom in scalar potential = 7.
 *
 * @params:
 *   The Yukawa sector is the same as in Base_Higgs. 
 *   
 *   mh: Lightest neutral CP even Higgs boson mass.
 *   mH: Heaviest neutral CP even Higgs boson mass.
 *   Z4, Z5, Z7: Real quartic couplings.
 *   cba: cos(beta-alpha)
 *   tanb: tan(beta)
 */
struct Base_hybrid : ThdmBasis {
  double mh, mH, cba, Z4, Z5, Z7, tanb;

  Eigen::Matrix3cd kU, rU, kD, rD, kL, rL;

  Base_hybrid() { type = HYBRID; xi = 0.; }

  // Overloading the << operator for ostream
  friend std::ostream &operator<<(std::ostream &oS, const Base_hybrid &hyb) {
    oS << "Hybrid basis:\n"
       << "mh = " << hyb.mh << std::endl
       << "mH = " << hyb.mH << std::endl
       << "Z4 = " << hyb.Z4 << std::endl
       << "Z5 = " << hyb.Z5 << std::endl
       << "Z7 =" << hyb.Z7 << std::endl
       << "cba = " << hyb.cba << std::endl
       << "(sba = " << std::sqrt(std::abs(1.-hyb.cba*hyb.cba)) << ")\n"
       << "tanb = " << hyb.tanb << std::endl;
     return oS;
  }
  
  // Sets parameters to random values.
  void generate_random_softCpConserved(const gsl_rng *rng);

  Base_generic convert_to_generic(const double &v2) const;
  Base_higgs convert_to_higgs(const double &v2) const;
  Base_invariant convert_to_invariant(const double &v2) const;
};